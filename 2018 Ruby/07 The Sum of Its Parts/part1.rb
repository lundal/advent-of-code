# In what order should the steps in your instructions be completed?

require 'set'

instructions = {}
instruction_order = []

File.read('input.txt').lines.each do |line|
  /Step (?<prequesite>\w+) must be finished before step (?<step>\w+) can begin/ =~ line

  instructions[prequesite] = Set.new unless instructions.key?(prequesite)
  instructions[step] = Set.new unless instructions.key?(step)

  instructions[step].add(prequesite)
end

while instruction_order.length < instructions.length
  next_instruction =
    instructions.keys
                .reject { |key| instruction_order.include?(key) }
                .select { |key| instructions[key].subset?(Set.new(instruction_order)) }
                .min

  instruction_order.append(next_instruction)
end

puts instruction_order.join
