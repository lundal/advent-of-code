# How long will it take to complete all of the steps?

require 'set'

instructions = {}
instruction_order = []
instruction_queue = []
time = 0

File.read('input.txt').lines.each do |line|
  /Step (?<prequesite>\w+) must be finished before step (?<step>\w+) can begin/ =~ line

  instructions[prequesite] = Set.new unless instructions.key?(prequesite)
  instructions[step] = Set.new unless instructions.key?(step)

  instructions[step].add(prequesite)
end

while instruction_order.length < instructions.length
  instructions.keys
              .reject { |key| instruction_queue.map { |w| w[:inst] }.include?(key) }
              .reject { |key| instruction_order.include?(key) }
              .select { |key| instructions[key].subset?(Set.new(instruction_order)) }
              .sort
              .each do |inst|
                next unless instruction_queue.length < 5
                instruction_queue.append(inst: inst, time: inst.ord - 64 + 60)
              end


  puts "Done: #{instruction_order.join} | Queue: #{instruction_queue}"

  instruction_queue.each { |w| w[:time] -= 1 }
  time += 1

  instruction_queue.select { |w| w[:time].zero? }.each { |w| instruction_order.append(w[:inst]) }
  instruction_queue.delete_if { |w| w[:time].zero? }
end

puts time
