# What is the first frequency your device reaches twice?

require('set')

frequency = 0
frequencies = Set.new([frequency])
changes = File.read('input.txt').lines.map(&:to_i)

loop do
  changes.each do |c|
    frequency += c
    if frequencies.add?(frequency).nil?
      puts frequency
      exit
    end
  end
end
