# What is the ID of the guard you chose multiplied by the minute you chose?

current_guard = 0
fell_asleep = 0
sleep_minutes = {}

File.read('input.txt').lines.sort.each do |line|
  /\[(?<year>\d+)-(?<month>\d+)-(?<day>\d+) (?<hour>\d+):(?<minute>\d+)\] (Guard #(?<new_guard>\d+) .*|(?<event>.*))/ =~ line

  year = year.to_i
  month = month.to_i
  day = day.to_i
  hour = hour.to_i
  minute = minute.to_i
  new_guard = new_guard.to_i

  current_guard = new_guard if new_guard > 0

  if !event.nil? && event.include?('falls asleep')
    fell_asleep = minute
  end

  if !event.nil? && event.include?('wakes up')
    (fell_asleep..minute - 1).each do |m|
      unless sleep_minutes.key?(current_guard)
        sleep_minutes[current_guard] = {}
        sleep_minutes[current_guard].default = 0
      end
      sleep_minutes[current_guard][m] += 1
    end
  end
end

chosen_guard = 0
chosen_max = 0
chosen_minute = 0

sleep_minutes.each do |guard, minutes|
  max = minutes.values.max
  minute = minutes.key(minutes.values.max)

  if max > chosen_max
    chosen_guard = guard
    chosen_max = max
    chosen_minute = minute
  end
end

puts chosen_guard * chosen_minute
