# What is the value of the root node?

def parseNode(input)
  num_subnodes = input[0]
  num_metadata = input[1]
  remaining = input.drop(2)

  # Subnodes
  subnodes = []
  (1..num_subnodes).each do
    subnode, rem = parseNode(remaining)
    subnodes.append(subnode)
    remaining = rem
  end

  # Metadata
  metadata = remaining.slice(0, num_metadata)
  remaining = remaining.drop(num_metadata)

  [{ subnodes: subnodes, metadata: metadata }, remaining]
end

def calcValue(node)
  return node[:metadata].sum if node[:subnodes].empty?

  node[:metadata]
    .map { |meta| meta - 1 }
    .select { |id| id >= 0 && id < node[:subnodes].length }
    .map { |id| calcValue(node[:subnodes][id]) }
    .sum
end

input = File.read('input.txt').split(' ').map(&:to_i)
root_node, = parseNode(input)

puts "#{root_node}"
puts calcValue(root_node)

