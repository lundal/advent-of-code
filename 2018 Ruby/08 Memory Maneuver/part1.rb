# What is the sum of all metadata entries?

def parseNode(input)
  num_subnodes = input[0]
  num_metadata = input[1]
  remaining = input.drop(2)

  # Subnodes
  subnodes = []
  (1..num_subnodes).each do
    subnode, rem = parseNode(remaining)
    subnodes.append(subnode)
    remaining = rem
  end

  # Metadata
  metadata = remaining.slice(0, num_metadata)
  remaining = remaining.drop(num_metadata)

  [{ subnodes: subnodes, metadata: metadata }, remaining]
end

def sumMetadata(node)
  node[:metadata].sum + node[:subnodes].map { |sub| sumMetadata(sub) }.sum
end

input = File.read('input.txt').split(' ').map(&:to_i)
root_node, = parseNode(input)

puts "#{root_node}"
puts sumMetadata(root_node)

