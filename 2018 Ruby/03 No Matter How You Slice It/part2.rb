# What is the ID of the only claim that doesn't overlap?

require('set')

claims = {}
claims.default = []

exclusive_claims = Set.new([])

File.read('input.txt').lines.each do |line|
  /#(?<id>\d+) @ (?<x>\d+),(?<y>\d+): (?<w>\d+)x(?<h>\d+)/ =~ line

  id = id.to_i
  x = x.to_i
  y = y.to_i
  w = w.to_i
  h = h.to_i

  exclusive_claims.add(id)

  (x..x + w - 1).each do |xx|
    (y..y + h - 1).each do |yy|
      sq = "#{xx}x#{yy}"
      exclusive = !claims.key?(sq)

      claims[sq] = [] unless claims.key?(sq)
      claims[sq] = claims[sq].push(id)

      exclusive_claims.subtract(claims[sq]) unless exclusive
    end
  end
end

puts claims
puts claims.keep_if { |_k, v| v.length > 1 } .length

puts exclusive_claims
