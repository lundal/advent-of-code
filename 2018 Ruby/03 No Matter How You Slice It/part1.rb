# How many square inches of fabric are within two or more claims?

claims = {}
claims.default = []

File.read('input.txt').lines.each do |line|
  /#(?<id>\d+) @ (?<x>\d+),(?<y>\d+): (?<w>\d+)x(?<h>\d+)/ =~ line

  id = id.to_i
  x = x.to_i
  y = y.to_i
  w = w.to_i
  h = h.to_i

  (x..x + w - 1).each do |xx|
    (y..y + h - 1).each do |yy|
      sq = "#{xx}x#{yy}"
      claims[sq] = [] unless claims.key?(sq)
      claims[sq] = claims[sq].push(id)
    end
  end
end

puts claims
puts claims.keep_if { |_k, v| v.length > 1 } .length
