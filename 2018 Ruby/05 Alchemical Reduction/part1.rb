# How many units remain after fully reacting the polymer you scanned?

reactors = []

('a'..'z').each do |r|
  reactors.push("#{r}#{r.capitalize}")
  reactors.push("#{r.capitalize}#{r}")
end

File.read('input.txt').lines.each do |line|
  polymer = line.delete("\n")

  oldlen = 0
  newlen = polymer.length

  while oldlen != newlen
    reactors.each do |r|
      polymer = polymer.gsub(r, '')
    end

    oldlen = newlen
    newlen = polymer.length
  end

  puts polymer.length
end
