# What is the length of the shortest polymer you can produce?

reactors = []

('a'..'z').each do |r|
  reactors.push("#{r}#{r.capitalize}")
  reactors.push("#{r.capitalize}#{r}")
end

lengths = []

File.read('input.txt').lines.each do |line|
  ('a'..'z').each do |x|
    polymer = line.delete("\n#{x}#{x.capitalize}")

    oldlen = 0
    newlen = polymer.length

    while oldlen != newlen
      reactors.each do |r|
        polymer = polymer.gsub(r, '')
      end

      oldlen = newlen
      newlen = polymer.length
    end

    lengths.push(polymer.length)
  end
end

puts lengths.min
