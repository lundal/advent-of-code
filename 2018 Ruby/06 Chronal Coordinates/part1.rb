# What is the size of the largest area that isn't infinite?

require 'set'

def coords(x, y)
  "#{x}x#{y}"
end

def read(file)
  grid = {}
  num = 1

  File.read(file).lines.each do |line|
    /(?<x>\d+), (?<y>\d+)/ =~ line

    x = x.to_i
    y = y.to_i

    if x > 0 && y > 0
      grid[coords(x, y)] = num
      num += 1
    end
  end

  grid
end

def grow(grid, max)
  new_grid = grid.clone
  (0..max).each do |y|
    (0..max).each do |x|
      next unless grid[coords(x, y)].nil?

      up = grid[coords(x, y - 1)]
      down = grid[coords(x, y + 1)]
      left = grid[coords(x - 1, y)]
      right = grid[coords(x + 1, y)]

      adjacent = [up, down, left, right].reject(&:nil?)

      # print "#{x}x#{y}: #{adjacent}\n"

      new_grid[coords(x, y)] = 0 if adjacent.uniq.length > 1
      new_grid[coords(x, y)] = adjacent.first if adjacent.uniq.length == 1
    end
  end

  new_grid
end

def show(grid, max)
  (0..max).each do |y|
    (0..max).each do |x|
      val = grid[coords(x, y)]
      print '.' if val.nil?
      print val unless val.nil?
    end
    print "\n"
  end
  print "\n"
end

def filled?(grid, max)
  (0..max).each do |y|
    (0..max).each do |x|
      return false if grid[coords(x, y)].nil?
    end
  end
  true
end

def edge_values(grid, max)
  vals = Set.new
  (0..max).each do |y|
    (0..max).each do |x|
      next unless x == 0 || x == max || y == 0 or y == max

      vals.add grid[coords(x, y)]
    end
  end
  vals
end

grid = read('input.txt')
max = 400

until filled?(grid, max)
  grid = grow(grid, max)
  # show(grid, max)
  puts "i"
end

reject_vals = edge_values(grid, max)
grid = grid.reject { |_, v| reject_vals.include?(v) }
# show(grid, max)

puts grid.values.group_by { |v| v }.map { |c, v| v.length }.max