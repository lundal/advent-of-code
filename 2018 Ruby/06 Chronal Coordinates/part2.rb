# What is the size of the region containing all locations which have a total distance to all given coordinates of less than 10000?

points = []

File.read('input.txt').lines.each do |line|
  /(?<x>\d+), (?<y>\d+)/ =~ line

  x = x.to_i
  y = y.to_i

  points.append([x, y]) if x > 0 && y > 0
end

safe_points = 0
max = 400

(0..max).each do |y|
  (0..max).each do |x|
    total_distance = points.map { |px, py| (x - px).abs + (y - py).abs }.sum
    safe_points += 1 if total_distance < 10_000
  end
end

puts safe_points