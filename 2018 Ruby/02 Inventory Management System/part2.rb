# What letters are common between the two correct box IDs?

File.read('input.txt').lines.combination(2)
    .each do |a, b|
      different = 0
      similar = ''

      a.chars.zip(b.chars).each do |ac, bc|
        different += 1 if ac != bc
        similar += ac.to_s if ac == bc
      end
      puts similar if different == 1
    end
