# What is the checksum for your list of box IDs?

double = 0
triple = 0

File.read('input.txt').lines.each do |s|
  counts = s.chars.group_by(&:downcase).map { |_k, v| v.length }
  double += 1 if counts.member?(2)
  triple += 1 if counts.member?(3)
end

puts double * triple
