# Rust

## Getting Started

- Follow the instructions at https://www.rust-lang.org/tools/install

## Running the Code

- `cargo test -r dayXX -- --show-output`
