use crate::common::*;

fn part1(input: &str) -> i32 {
    input.lines()
        .map(|line| {
            let tokens: Vec<&str> = line.split(" ").collect();
            match tokens[..] {
                ["A", "X"] => 1 + 3,
                ["A", "Y"] => 2 + 6,
                ["A", "Z"] => 3 + 0,

                ["B", "X"] => 1 + 0,
                ["B", "Y"] => 2 + 3,
                ["B", "Z"] => 3 + 6,

                ["C", "X"] => 1 + 6,
                ["C", "Y"] => 2 + 0,
                ["C", "Z"] => 3 + 3,

                _ => 0,
            }
        })
        .sum()
}

fn part2(input: &str) -> i32 {
    input.lines()
        .map(|line| {
            let tokens: Vec<&str> = line.split(" ").collect();
            match tokens[..] {
                ["A", "X"] => 3 + 0,
                ["A", "Y"] => 1 + 3,
                ["A", "Z"] => 2 + 6,

                ["B", "X"] => 1 + 0,
                ["B", "Y"] => 2 + 3,
                ["B", "Z"] => 3 + 6,

                ["C", "X"] => 2 + 0,
                ["C", "Y"] => 3 + 3,
                ["C", "Z"] => 1 + 6,

                _ => 0,
            }
        })
        .sum()
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
