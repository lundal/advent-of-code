use std::cmp::{max, min};

use crate::common::*;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum Tile {
    Air,
    Rock,
    Sand,
}

fn parse_lines(input: &str) -> Vec<Vec<(usize, usize)>> {
    input.lines()
        .map(|line| {
            line.split(" -> ")
                .map(|point| {
                    let nums: Vec<usize> = point.split(",")
                        .map(|s| s.parse().expect("Not an int"))
                        .collect();
                    (nums[0], nums[1])
                })
                .collect()
        })
        .collect()
}

fn to_cave(lines: Vec<Vec<(usize, usize)>>) -> [[Tile; 1000]; 1000] {
    let mut cave = [[Tile::Air; 1000]; 1000];
    for line in lines {
        line.iter().zip(line.iter().skip(1))
            .for_each(|(&(ax, ay), &(bx, by))| {
                if ax == bx {
                    for y in min(ay, by)..=max(ay, by) {
                        cave[ax][y] = Tile::Rock
                    }
                }
                if ay == by {
                    for x in min(ax, bx)..=max(ax, bx) {
                        cave[x][ay] = Tile::Rock
                    }
                }
            });
    }
    cave
}

fn next(cave: &[[Tile; 1000]; 1000]) -> Option<(usize, usize)> {
    let mut x = 500;
    let mut y = 0;
    while y < 1000 - 1 {
        if cave[x][y + 1] == Tile::Air {
            y += 1;
        } else if cave[x - 1][y + 1] == Tile::Air {
            x -= 1;
            y += 1;
        } else if cave[x + 1][y + 1] == Tile::Air {
            x += 1;
            y += 1;
        } else {
            return Some((x, y));
        }
    }
    None
}

fn part1(input: &str) -> usize {
    let lines = parse_lines(input);
    let mut cave = to_cave(lines);

    let mut i = 0;
    loop {
        if let Some((x, y)) = next(&cave) {
            cave[x][y] = Tile::Sand;
            i += 1;
        } else {
            break;
        }
    }
    i
}

fn part2(input: &str) -> usize {
    let lines = parse_lines(input);

    let max_y = lines.iter()
        .flat_map(|points| points.iter())
        .map(|&(_, y)| y)
        .max()
        .unwrap();

    let mut cave = to_cave(lines);

    for x in 0..1000 {
        cave[x][max_y + 2] = Tile::Rock
    }

    let mut i = 0;
    loop {
        if cave[500][0] == Tile::Sand {
            break;
        }
        if let Some((x, y)) = next(&cave) {
            cave[x][y] = Tile::Sand;
            i += 1;
        }
    }
    i
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
