use regex::Regex;

use crate::common::*;

#[derive(Copy, Clone, Debug)]
struct Sensor {
    position: Vec2<i64>,
    beacon: Vec2<i64>,
    range: i64,
}

fn parse_sensors(input: &str) -> Vec<Sensor> {
    let re = Regex::new(r"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)").unwrap();
    input.lines()
        .map(|line| {
            let captures = re.captures(line).unwrap();
            let x = captures.get(1).unwrap().as_str().parse().unwrap();
            let y = captures.get(2).unwrap().as_str().parse().unwrap();
            let bx = captures.get(3).unwrap().as_str().parse().unwrap();
            let by = captures.get(4).unwrap().as_str().parse().unwrap();
            Sensor {
                position: Vec2::new(x, y),
                beacon: Vec2::new(bx, by),
                range: manhattan(Vec2::new(x, y) - Vec2::new(bx, by)),
            }
        })
        .collect()
}

fn manhattan(vec: Vec2<i64>) -> i64 {
    vec.x.abs() + vec.y.abs()
}

fn part1(input: &str, y: i64) -> i64 {
    let sensors: Vec<Sensor> = parse_sensors(input);
    let mut covered: i64 = 0;
    'x: for x in -2000000..=8000000 {
        let mut is_covered = false;
        for sensor in sensors.iter() {
            let target = Vec2::new(x, y);
            if target == sensor.beacon {
                continue 'x;
            }
            let target_dist = manhattan(target - sensor.position);
            if target_dist <= sensor.range {
                is_covered = true;
            }
        }
        if is_covered {
            covered += 1
        }
    }
    covered
}

fn part2(input: &str, max: i64) -> i64 {
    let mut sensors: Vec<Sensor> = parse_sensors(input);
    sensors.sort_by(|a, b| b.range.cmp(&a.range));

    let mut i = 0;
    'i: while i <= max * max {
        let x = i / max;
        let y = i % max;
        let target = Vec2::new(x, y);
        for sensor in sensors.iter() {
            let target_dist = manhattan(target - sensor.position);
            if target_dist <= sensor.range {
                let diff = sensor.range - target_dist;
                if x + diff > max {
                    i += max - x;
                } else if diff > 0 {
                    i += diff;
                } else {
                    i += 1;
                }
                continue 'i;
            }
        }
        return x*4000000 + y;
    }
    panic!("Not found :(")
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path), 10))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path), 2000000))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path), 20))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path), 4000000))
}
