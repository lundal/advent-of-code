use regex::Regex;

use crate::common::*;

fn parse(input: &str) -> (Vec<Vec<char>>, Vec<(usize, usize, usize)>) {
    let chunks: Vec<&str> = input.split("\n\n").collect();
    let stacks = parse_stacks(chunks[0]);
    let instructions = parse_instructions(chunks[1]);
    (stacks, instructions)
}

fn parse_stacks(input: &str) -> Vec<Vec<char>> {
    let mut stacks = Vec::new();
    input.lines()
        .rev()
        .for_each(|line| {
            line.chars().skip(1).step_by(4).enumerate().for_each(|(i, c)| {
                if stacks.len() < i + 1 {
                    stacks.push(Vec::new())
                }
                if c.is_uppercase() {
                    stacks[i].push(c);
                }
            });
        });
    stacks
}

fn parse_instructions(input: &str) -> Vec<(usize, usize, usize)> {
    let re = Regex::new(r"move (\d+) from (\d+) to (\d+)").unwrap();
    input.lines()
        .map(|line| {
            let captures = re.captures(line).unwrap();
            let count = captures.get(1).unwrap().as_str().parse().unwrap();
            let from = captures.get(2).unwrap().as_str().parse().unwrap();
            let to = captures.get(3).unwrap().as_str().parse().unwrap();
            (count, from, to)
        })
        .collect()
}

fn part1(input: &str) -> String {
    let (mut stacks, instructions) = parse(input);
    instructions.iter().copied().for_each(|(count, from, to)| {
        for _ in 0..count {
            let c = stacks[from - 1].pop().unwrap();
            stacks[to - 1].push(c);
        }
    });
    stacks.iter().map(|stack| stack.last().unwrap()).collect()
}

fn part2(input: &str) -> String {
    let (mut stacks, instructions) = parse(input);
    instructions.iter().copied().for_each(|(count, from, to)| {
        let mut temp = Vec::new();
        for _ in 0..count {
            let c = stacks[from - 1].pop().unwrap();
            temp.push(c);
        }
        temp.iter().copied().rev().for_each(|c| stacks[to - 1].push(c));
    });
    stacks.iter().map(|stack| stack.last().unwrap()).collect()
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
