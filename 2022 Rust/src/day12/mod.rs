use std::cmp::Ordering;
use std::collections::BinaryHeap;

use crate::common::*;

fn parse_heightmap(input: &str) -> (Vec<Vec<usize>>, Vec2<usize>, Vec2<usize>) {
    let mut start: Vec2<usize> = Vec2::new(0, 0);
    let mut end: Vec2<usize> = Vec2::new(0, 0);
    let heightmap = input.lines()
        .enumerate()
        .map(|(y, line)| {
            line.chars()
                .enumerate()
                .map(|(x, char)| {
                    if char.is_ascii_lowercase() {
                        get_height(char)
                    } else if char == 'S' {
                        start = Vec2::new(x, y);
                        get_height('a')
                    } else if char == 'E' {
                        end = Vec2::new(x, y);
                        get_height('z')
                    } else {
                        panic!("bad input")
                    }
                })
                .collect()
        })
        .collect();
    (heightmap, start, end)
}

fn get_height(c: char) -> usize {
    (c as usize) - 97
}

fn get_next(heightmap: &Vec<Vec<usize>>, position: Vec2<usize>) -> Vec<Vec2<usize>> {
    let mut adjacent = Vec::new();
    if position.y > 0 {
        adjacent.push(Vec2::new(position.x, position.y - 1))
    }
    if position.y < heightmap.len() - 1 {
        adjacent.push(Vec2::new(position.x, position.y + 1))
    }
    if position.x > 0 {
        adjacent.push(Vec2::new(position.x - 1, position.y))
    }
    if position.x < heightmap[0].len() - 1 {
        adjacent.push(Vec2::new(position.x + 1, position.y))
    }
    adjacent.iter()
        .copied()
        .filter(|adjacent_position| {
            let height = heightmap[position.y][position.x];
            let adjacent_height = heightmap[adjacent_position.y][adjacent_position.x];
            adjacent_height <= height + 1
        })
        .collect()
}

#[derive(Copy, Clone, Eq, PartialEq)]
struct State {
    distance: usize,
    position: Vec2<usize>,
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other.distance.cmp(&self.distance)
            .then_with(|| self.position.x.cmp(&other.position.x))
            .then_with(|| self.position.y.cmp(&other.position.y))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn shortest_path(heightmap: &Vec<Vec<usize>>, start: Vec2<usize>, end: Vec2<usize>) -> Option<usize> {
    let mut distances: Vec<Vec<usize>> = heightmap.iter()
        .map(|row| row.iter().map(|_| usize::MAX).collect())
        .collect();
    let mut heap: BinaryHeap<State> = BinaryHeap::new();

    distances[start.y][start.x] = 0;
    heap.push(State { distance: 0, position: start });

    while let Some(State { position, distance }) = heap.pop() {
        if position == end {
            return Some(distance);
        }
        if distance > distances[position.y][position.x] {
            continue;
        }
        for next in get_next(heightmap, position) {
            if distance + 1 < distances[next.y][next.x] {
                distances[next.y][next.x] = distance + 1;
                heap.push(State { distance: distance + 1, position: next })
            }
        }
    }
    None
}

fn part1(input: &str) -> usize {
    let (heightmap, start, end) = parse_heightmap(input);
    let steps = shortest_path(&heightmap, start, end).unwrap();
    steps
}

fn part2(input: &str) -> usize {
    let (heightmap, _, end) = parse_heightmap(input);
    let mut least_steps = usize::MAX;
    for (y, row) in heightmap.iter().enumerate() {
        for (x, height) in row.iter().enumerate() {
            if *height == 0 {
                if let Some(steps) = shortest_path(&heightmap, Vec2::new(x, y), end) {
                    if least_steps > steps {
                        least_steps = steps;
                    }
                }
            }
        }
    }
    least_steps
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
