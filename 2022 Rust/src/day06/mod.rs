use std::collections::HashSet;

use crate::common::*;

fn part1(input: &str) -> usize {
    let window = 4;
    for i in 0..input.len() - window {
        let chars: HashSet<char> = input.chars().skip(i).take(window).collect();
        if chars.len() == window {
            return i + window;
        }
    }
    return 0;
}

fn part2(input: &str) -> usize {
    let window = 14;
    for i in 0..input.len() - window {
        let chars: HashSet<char> = input.chars().skip(i).take(window).collect();
        if chars.len() == window {
            return i + window;
        }
    }
    return 0;
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
