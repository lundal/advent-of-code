use crate::common::*;

fn parse_forest(input: &str) -> Vec<Vec<usize>> {
    input.lines()
        .map(|line|
            line.chars()
                .map(|c| c.to_string().parse().unwrap())
                .collect())
        .collect()
}

fn is_visible_from_edge(forest: &Vec<Vec<usize>>, y: usize, x: usize) -> bool {
    let y_max = forest.len() - 1;
    let x_max = forest[0].len() - 1;
    if y == 0 || y == y_max || x == 0 || x == x_max {
        return true;
    }
    let top = (0..=(y - 1)).all(|y_other| forest[y_other][x] < forest[y][x]);
    let bot = ((y + 1)..=y_max).all(|y_other| forest[y_other][x] < forest[y][x]);
    let left = (0..=(x - 1)).all(|x_other| forest[y][x_other] < forest[y][x]);
    let right = ((x + 1)..=x_max).all(|x_other| forest[y][x_other] < forest[y][x]);

    top || bot || left || right
}

fn scenic_score(forest: &Vec<Vec<usize>>, y: usize, x: usize) -> usize {
    let y_max = forest.len() - 1;
    let x_max = forest[0].len() - 1;

    // :(
    // let top = (0..=y).rev().skip(1).take_while(|&y_other| forest[y_other][x] < forest[y][x]).count();
    // let bot = (y..=y_max).skip(1).take_while(|&y_other| forest[y_other][x] < forest[y][x]).count();
    // let left = (0..=x).rev().skip(1).take_while(|&x_other| forest[y][x_other] < forest[y][x]).count();
    // let right = (x..=x_max).skip(1).take_while(|&x_other| forest[y][x_other] < forest[y][x]).count();

    let mut top = 0;
    for y_other in (0..=y).rev().skip(1) {
        if forest[y_other][x] < forest[y][x] {
            top += 1;
        } else {
            top += 1;
            break;
        }
    }

    let mut bot = 0;
    for y_other in (y..=y_max).skip(1) {
        if forest[y_other][x] < forest[y][x] {
            bot += 1;
        } else {
            bot += 1;
            break;
        }
    }

    let mut left = 0;
    for x_other in (0..=x).rev().skip(1) {
        if forest[y][x_other] < forest[y][x] {
            left += 1;
        } else {
            left += 1;
            break;
        }
    }

    let mut right = 0;
    for x_other in (x..=x_max).skip(1) {
        if forest[y][x_other] < forest[y][x] {
            right += 1;
        } else {
            right += 1;
            break;
        }
    }

    //println!("y={} x={} top={} bot={} left={} right={}", y, x, top, bot, left, right);
    top * bot * left * right
}

fn part1(input: &str) -> usize {
    let forest: Vec<Vec<usize>> = parse_forest(input);
    let visible: Vec<Vec<bool>> = forest.iter()
        .enumerate()
        .map(|(y, row)|
            row.iter()
                .enumerate()
                .map(|(x, _)| is_visible_from_edge(&forest, y, x))
                .collect()
        )
        .collect();
    visible.iter().map(|row| row.iter().filter(|visible| **visible).count()).sum()
}

fn part2(input: &str) -> usize {
    let forest: Vec<Vec<usize>> = parse_forest(input);
    let scores: Vec<Vec<usize>> = forest.iter()
        .enumerate()
        .map(|(y, row)|
            row.iter()
                .enumerate()
                .map(|(x, _)| scenic_score(&forest, y, x))
                .collect()
        )
        .collect();
    scores.iter().map(|row| *row.iter().max().unwrap()).max().unwrap()
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
