use crate::common::*;

fn part1(input: &str) -> i32 {
    let mut sum = 0;
    let mut max = 0;

    for line in input.lines() {
        if line.is_empty() {
            if sum > max {
                max = sum;
            }
            sum = 0;
        } else {
            let calories = parse_i32(line);
            sum += calories
        }
    }

    return max;
}

fn part2(input: &str) -> i32 {
    let mut calories: Vec<i32> = vec![];

    let mut sum = 0;
    for line in input.lines() {
        if line.is_empty() {
            calories.push(sum);
            sum = 0;
        } else {
            let calories = parse_i32(line);
            sum += calories
        }
    }

    calories.sort();
    calories.reverse();

    return calories.iter().take(3).sum();
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
