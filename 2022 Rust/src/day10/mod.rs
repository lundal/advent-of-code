use crate::common::*;

enum Instruction {
    Addx { value: i32 },
    Noop,
}

fn parse_instructions(input: &str) -> Vec<Instruction> {
    input.lines()
        .flat_map(|line| {
            let words: Vec<&str> = line.split(" ").collect();
            match words[0] {
                "addx" => vec![
                    Instruction::Noop,
                    Instruction::Addx { value: words[1].parse().unwrap() },
                ],
                _ => vec![
                    Instruction::Noop
                ],
            }
        })
        .collect()
}

fn part1(input: &str) -> i32 {
    let instructions = parse_instructions(input);

    let mut x: i32 = 1;
    let mut sum: i32 = 0;

    for cycle in 1..=220 {
        if cycle % 40 == 20 {
            sum += cycle * x;
        }
        match &instructions[(cycle - 1) as usize] {
            Instruction::Addx { value } => { x += value }
            Instruction::Noop => {}
        }
    }

    sum
}

fn part2(input: &str) -> String {
    let instructions = parse_instructions(input);

    let mut x: i32 = 1;
    let mut output: String = String::from("\n");

    for cycle in 1..=240 {
        if (((cycle - 1) % 40) - x).abs() <= 1 {
            output += "#";
        } else {
            output += ".";
        }
        if cycle % 40 == 0 {
            output += "\n";
        }
        match &instructions[(cycle - 1) as usize] {
            Instruction::Addx { value } => { x += value }
            Instruction::Noop => {}
        }
    }

    output
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
