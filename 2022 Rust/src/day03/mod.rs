use std::collections::HashSet;

use crate::common::*;

fn part1(input: &str) -> i32 {
    input.lines()
        .map(|line| {
            let (first_half, second_half) = line.split_at(line.len() / 2);

            let first_items: HashSet<char> = first_half.chars().collect();
            let second_items: HashSet<char> = second_half.chars().collect();

            let item: char = first_items
                .intersection(&second_items)
                .copied().collect::<Vec<char>>()
                .first().copied().unwrap();

            if item.is_ascii_lowercase() {
                (item as i32) - 96
            } else {
                (item as i32) - 64 + 26
            }
        })
        .sum()
}

fn part2(input: &str) -> i32 {
    input.lines()
        .array_chunks::<3>()
        .map(|[line1, line2, line3]| {
            let first_items: HashSet<char> = line1.chars().collect();
            let second_items: HashSet<char> = line2.chars().collect();
            let third_items: HashSet<char> = line3.chars().collect();

            let item: char = first_items
                .intersection(&second_items).copied().collect::<HashSet<char>>()
                .intersection(&third_items).copied().collect::<Vec<char>>()
                .first().copied().unwrap();

            if item.is_ascii_lowercase() {
                (item as i32) - 96
            } else {
                (item as i32) - 64 + 26
            }
        })
        .sum()
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
