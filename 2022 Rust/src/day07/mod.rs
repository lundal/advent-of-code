use crate::common::*;

#[derive(Debug)]
struct Directory {
    name: String,
    dirs: Vec<Directory>,
    files: Vec<File>,
}

#[derive(Debug)]
struct File {
    name: String,
    size: usize,
}

impl Directory {
    fn size(&self) -> usize {
        self.dirs.iter().map(|dir| dir.size()).sum::<usize>()
            + self.files.iter().map(|file| file.size).sum::<usize>()
    }

    fn subdirs(&self) -> Vec<&Directory> {
        let mut dirs = vec![self];
        self.dirs.iter()
            .flat_map(|dir| dir.subdirs())
            .for_each(|dir| dirs.push(dir));
        dirs
    }
}

fn parse(input: &str) -> Directory {
    let mut root: Directory = Directory { name: "/".into(), dirs: Vec::new(), files: Vec::new() };
    let mut path: Vec<String> = Vec::new();

    for line in input.lines().skip(1) {
        let parts: Vec<&str> = line.split(" ").collect();
        if parts[0] == "$" {
            if parts[1] == "cd" {
                if parts[2] == ".." {
                    path.pop();
                } else {
                    path.push(parts[2].into());
                }
            }
        } else {
            if parts[0] == "dir" {
                get_directory(&mut root, &path).dirs.push(Directory {
                    name: parts[1].into(),
                    dirs: Vec::new(),
                    files: Vec::new(),
                })
            } else {
                get_directory(&mut root, &path).files.push(File {
                    name: parts[1].into(),
                    size: parts[0].parse().unwrap(),
                })
            }
        }
    }

    root
}

fn get_directory<'a>(root: &'a mut Directory, path: &Vec<String>) -> &'a mut Directory {
    let mut current_dir: &mut Directory = root;
    for path_segment in path {
        current_dir = current_dir.dirs.iter_mut()
            .find(|dir| dir.name == *path_segment)
            .unwrap();
    }
    current_dir
}

fn part1(input: &str) -> usize {
    let root = parse(input);
    root.subdirs().iter()
        .map(|dir| dir.size())
        .filter(|size| *size <= 100000)
        .sum()
}

fn part2(input: &str) -> usize {
    let root = parse(input);

    let unused = 70000000 - root.size();
    let to_delete = 30000000 - unused;

    let mut candiate_dirs: Vec<usize> = root.subdirs().iter()
        .map(|dir| dir.size())
        .filter(|size| *size >= to_delete)
        .collect();

    candiate_dirs.sort();
    candiate_dirs.first().copied().unwrap()
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
