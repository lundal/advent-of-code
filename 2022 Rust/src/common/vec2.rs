use std::ops::{Add, Div, Mul, Neg, Sub};

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct Vec2<T> {
    pub x: T,
    pub y: T,
}

impl<T> Vec2<T> where T: Copy + Clone {
    pub const fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T: From<i32> + Copy + Clone> Vec2<T> {
    pub fn zero() -> Self {
        Self { x: 0.into(), y: 0.into() }
    }
    pub fn unit() -> Self {
        Self { x: 1.into(), y: 1.into() }
    }
}

impl<T> Vec2<T> where T: Add<T, Output=T> + Mul<T, Output=T> + Copy + Clone {
    pub fn length_squared(&self) -> T {
        self.x * self.x + self.y * self.y
    }
}

impl<T: Into<f64> + Copy + Clone> Vec2<T> {
    pub fn length(&self) -> f64 {
        let x = self.x.into();
        let y = self.y.into();
        (x * x + y * y).sqrt()
    }
    pub fn normalize(&self) -> Vec2<f64> {
        let length = self.length();
        Vec2 {
            x: self.x.into() / length,
            y: self.y.into() / length,
        }
    }
}

impl<T> Add<Vec2<T>> for Vec2<T> where T: Add<T, Output=T> + Copy + Clone {
    type Output = Vec2<T>;
    fn add(self, other: Vec2<T>) -> Self::Output {
        Vec2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<T> Add<&Vec2<T>> for &Vec2<T> where T: Add<T, Output=T> + Copy + Clone {
    type Output = Vec2<T>;
    fn add(self, other: &Vec2<T>) -> Self::Output {
        Vec2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<T> Sub<Vec2<T>> for Vec2<T> where T: Sub<T, Output=T> + Copy + Clone {
    type Output = Vec2<T>;
    fn sub(self, other: Vec2<T>) -> Self::Output {
        Vec2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<T> Sub<&Vec2<T>> for &Vec2<T> where T: Sub<T, Output=T> + Copy + Clone {
    type Output = Vec2<T>;
    fn sub(self, other: &Vec2<T>) -> Self::Output {
        Vec2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<T> Mul<T> for Vec2<T> where T: Mul<T, Output=T> + Copy + Clone {
    type Output = Vec2<T>;
    fn mul(self, other: T) -> Self::Output {
        Vec2 {
            x: self.x * other,
            y: self.y * other,
        }
    }
}

impl<T> Mul<T> for &Vec2<T> where T: Mul<T, Output=T> + Copy + Clone {
    type Output = Vec2<T>;
    fn mul(self, other: T) -> Self::Output {
        Vec2 {
            x: self.x * other,
            y: self.y * other,
        }
    }
}

impl<T> Div<T> for Vec2<T> where T: Div<T, Output=T> + Copy + Clone {
    type Output = Vec2<T>;
    fn div(self, other: T) -> Self::Output {
        Vec2 {
            x: self.x / other,
            y: self.y / other,
        }
    }
}

impl<T> Div<T> for &Vec2<T> where T: Div<T, Output=T> + Copy + Clone {
    type Output = Vec2<T>;
    fn div(self, other: T) -> Self::Output {
        Vec2 {
            x: self.x / other,
            y: self.y / other,
        }
    }
}

impl<T> Neg for Vec2<T> where T: Neg<Output=T> + Copy + Clone {
    type Output = Vec2<T>;
    fn neg(self) -> Self::Output {
        Vec2 {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl<T> Neg for &Vec2<T> where T: Neg<Output=T> + Copy + Clone {
    type Output = Vec2<T>;
    fn neg(self) -> Self::Output {
        Vec2 {
            x: -self.x,
            y: -self.y,
        }
    }
}
