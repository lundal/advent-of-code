pub fn parse_i32(value: &str) -> i32 {
    return value.parse().unwrap_or_else(|_| panic!("Could not parse '{value}' as i32"));
}

pub fn parse_bool(value: &str) -> bool {
    return value.parse().unwrap_or_else(|_| panic!("Could not parse '{value}' as bool"));
}
