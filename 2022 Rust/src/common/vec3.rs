use std::ops::{Add, Div, Mul, Neg, Sub};

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct Vec3<T> {
    pub x: T,
    pub y: T,
    pub z: T,
}

impl<T: Copy + Clone> Vec3<T> {
    pub const fn new(x: T, y: T, z: T) -> Self {
        Self { x, y, z }
    }
}

impl<T: From<i32> + Copy + Clone> Vec3<T> {
    pub fn zero() -> Self {
        Self { x: 0.into(), y: 0.into(), z: 0.into() }
    }
    pub fn unit() -> Self {
        Self { x: 1.into(), y: 1.into(), z: 1.into() }
    }
}

impl<T> Vec3<T> where T: Add<T, Output=T> + Mul<T, Output=T> + Copy + Clone {
    pub fn length_squared(&self) -> T {
        self.x * self.x + self.y * self.y + self.z * self.z
    }
}

impl<T: Into<f64> + Copy + Clone> Vec3<T> {
    pub fn length(&self) -> f64 {
        let x = self.x.into();
        let y = self.y.into();
        let z = self.z.into();
        (x * x + y * y + z * z).sqrt()
    }
    pub fn normalize(&self) -> Vec3<f64> {
        let length = self.length();
        Vec3 {
            x: self.x.into() / length,
            y: self.y.into() / length,
            z: self.z.into() / length,
        }
    }
}

impl<T> Add<Vec3<T>> for Vec3<T> where T: Add<T, Output=T> + Copy + Clone {
    type Output = Vec3<T>;
    fn add(self, other: Vec3<T>) -> Self::Output {
        Vec3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl<T> Add<&Vec3<T>> for &Vec3<T> where T: Add<T, Output=T> + Copy + Clone {
    type Output = Vec3<T>;
    fn add(self, other: &Vec3<T>) -> Self::Output {
        Vec3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl<T> Sub<Vec3<T>> for Vec3<T> where T: Sub<T, Output=T> + Copy + Clone {
    type Output = Vec3<T>;
    fn sub(self, other: Vec3<T>) -> Self::Output {
        Vec3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl<T> Sub<&Vec3<T>> for &Vec3<T> where T: Sub<T, Output=T> + Copy + Clone {
    type Output = Vec3<T>;
    fn sub(self, other: &Vec3<T>) -> Self::Output {
        Vec3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl<T> Mul<T> for Vec3<T> where T: Mul<T, Output=T> + Copy + Clone {
    type Output = Vec3<T>;
    fn mul(self, other: T) -> Self::Output {
        Vec3 {
            x: self.x * other,
            y: self.y * other,
            z: self.z * other,
        }
    }
}

impl<T> Mul<T> for &Vec3<T> where T: Mul<T, Output=T> + Copy + Clone {
    type Output = Vec3<T>;
    fn mul(self, other: T) -> Self::Output {
        Vec3 {
            x: self.x * other,
            y: self.y * other,
            z: self.z * other,
        }
    }
}

impl<T> Div<T> for Vec3<T> where T: Div<T, Output=T> + Copy + Clone {
    type Output = Vec3<T>;
    fn div(self, other: T) -> Self::Output {
        Vec3 {
            x: self.x / other,
            y: self.y / other,
            z: self.z / other,
        }
    }
}

impl<T> Div<T> for &Vec3<T> where T: Div<T, Output=T> + Copy + Clone {
    type Output = Vec3<T>;
    fn div(self, other: T) -> Self::Output {
        Vec3 {
            x: self.x / other,
            y: self.y / other,
            z: self.z / other,
        }
    }
}

impl<T> Neg for Vec3<T> where T: Neg<Output=T> + Copy + Clone {
    type Output = Vec3<T>;
    fn neg(self) -> Self::Output {
        Vec3 {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl<T> Neg for &Vec3<T> where T: Neg<Output=T> + Copy + Clone {
    type Output = Vec3<T>;
    fn neg(self) -> Self::Output {
        Vec3 {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}
