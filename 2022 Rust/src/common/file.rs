use std::fs;

pub fn read_file(path: &str) -> String {
    return fs::read_to_string(path).unwrap_or_else(|_| panic!("Failed to read file {path}"));
}
