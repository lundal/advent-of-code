use std::collections::HashSet;

use crate::common::*;

fn parse_moves(input: &str) -> Vec<Vec2<i32>> {
    input.lines()
        .flat_map(|line| {
            let parts: Vec<&str> = line.split(" ").collect();
            let direction = match parts[0] {
                "L" => Vec2::new(-1, 0),
                "R" => Vec2::new(1, 0),
                "U" => Vec2::new(0, -1),
                "D" => Vec2::new(0, 1),
                _ => Vec2::zero()
            };
            let amount: i32 = parts[1].parse().unwrap();
            (0..amount).map(|_| direction).collect::<Vec<Vec2<i32>>>()
        })
        .collect()
}

fn to_positions(moves: &Vec<Vec2<i32>>) -> Vec<Vec2<i32>> {
    let mut position = Vec2::zero();
    let mut positions = vec![position];
    for mov in moves {
        position = position + *mov;
        positions.push(position);
    }
    positions
}

fn follow(other_positions: &Vec<Vec2<i32>>) -> Vec<Vec2<i32>> {
    let mut position = Vec2::zero();
    let mut positions = vec![position];
    for other_position in other_positions {
        let delta = *other_position - position;
        let is_diagonal = delta.x != 0 && delta.y != 0;
        let length_squared = delta.x * delta.x + delta.y * delta.y;

        let mov = if is_diagonal && length_squared > 2 {
            let x = if delta.x < 0 { -1 } else if delta.x > 0 { 1 } else { 0 };
            let y = if delta.y < 0 { -1 } else if delta.y > 0 { 1 } else { 0 };
            Vec2::new(x, y)
        } else {
            let x = if delta.x < -1 { -1 } else if delta.x > 1 { 1 } else { 0 };
            let y = if delta.y < -1 { -1 } else if delta.y > 1 { 1 } else { 0 };
            Vec2::new(x, y)
        };

        position = position + mov;
        positions.push(position);
    }
    positions
}

fn unique(positions: &Vec<Vec2<i32>>) -> usize {
    positions.iter().copied().collect::<HashSet<Vec2<i32>>>().len()
}

fn part1(input: &str) -> usize {
    let moves = parse_moves(input);
    let head_positions = to_positions(&moves);
    let tail_positions = follow(&head_positions);
    unique(&tail_positions)
}

fn part2(input: &str) -> usize {
    let moves = parse_moves(input);
    let head_positions = to_positions(&moves);
    let tail_1_positions = follow(&head_positions);
    let tail_2_positions = follow(&tail_1_positions);
    let tail_3_positions = follow(&tail_2_positions);
    let tail_4_positions = follow(&tail_3_positions);
    let tail_5_positions = follow(&tail_4_positions);
    let tail_6_positions = follow(&tail_5_positions);
    let tail_7_positions = follow(&tail_6_positions);
    let tail_8_positions = follow(&tail_7_positions);
    let tail_9_positions = follow(&tail_8_positions);
    unique(&tail_9_positions)
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
