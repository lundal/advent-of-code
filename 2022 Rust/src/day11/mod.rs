use regex::Regex;

use crate::common::*;

#[derive(Debug, Clone)]
struct Monkey {
    items: Vec<usize>,
    operation: Operation,
    divider: usize,
    when_true: usize,
    when_false: usize,
}

#[derive(Debug, Clone)]
enum Operation {
    Add(usize),
    Mult(usize),
    Square,
}

fn parse_monkeys(input: &str) -> Vec<Monkey> {
    input.split("\n\n").map(|part| parse_monkey(part)).collect()
}

fn parse_monkey(input: &str) -> Monkey {
    let re = Regex::new(concat!(
    r"Monkey \d+:\s+",
    r"Starting items: (.+)\s+",
    r"Operation: new = old (.) (\w+)\s+",
    r"Test: divisible by (\d+)\s+",
    r"If true: throw to monkey (\d+)\s+",
    r"If false: throw to monkey (\d+)",
    )).unwrap();
    let captures = re.captures(input).unwrap();

    let items_str = captures.get(1).unwrap().as_str();
    let operator_str = captures.get(2).unwrap().as_str();
    let operand_str = captures.get(3).unwrap().as_str();
    let divider = captures.get(4).unwrap().as_str().parse().unwrap();
    let when_true = captures.get(5).unwrap().as_str().parse().unwrap();
    let when_false = captures.get(6).unwrap().as_str().parse().unwrap();

    let items: Vec<usize> = items_str
        .split(", ")
        .map(|word| word.parse().unwrap())
        .collect();

    let operation = match (operator_str, operand_str) {
        ("*", "old") => Operation::Square,
        ("*", n) => Operation::Mult(n.parse().unwrap()),
        ("+", n) => Operation::Add(n.parse().unwrap()),
        _ => panic!()
    };

    Monkey { items, operation, divider, when_true, when_false }
}

fn part1(input: &str) -> usize {
    let mut monkeys = parse_monkeys(input);
    let mut inspections: Vec<usize> = vec![0; monkeys.len()];

    for _ in 0..20 {
        for current_monkey in 0..monkeys.len() {
            let monkey = monkeys[current_monkey].clone();
            for item in &monkey.items {
                inspections[current_monkey] += 1;
                let new_item =
                    match monkey.operation {
                        Operation::Square => item * item,
                        Operation::Mult(n) => item * n,
                        Operation::Add(n) => item + n,
                    } / 3;
                let new_monkey =
                    if new_item % monkey.divider == 0 {
                        monkey.when_true
                    } else {
                        monkey.when_false
                    };
                monkeys[new_monkey].items.push(new_item);
            }
            monkeys[current_monkey].items = Vec::new();
        }
    }

    inspections.sort();
    inspections.reverse();

    inspections[0] * inspections[1]
}

fn part2(input: &str) -> usize {
    let mut monkeys = parse_monkeys(input);
    let mut inspections: Vec<usize> = vec![0; monkeys.len()];
    let common_divider: usize = monkeys.iter().map(|monkey| monkey.divider).product();

    for _ in 0..10000 {
        for current_monkey in 0..monkeys.len() {
            let monkey = monkeys[current_monkey].clone();
            for item in &monkey.items {
                inspections[current_monkey] += 1;
                let new_item =
                    match monkey.operation {
                        Operation::Square => item * item,
                        Operation::Mult(n) => item * n,
                        Operation::Add(n) => item + n,
                    } % common_divider;
                let new_monkey =
                    if new_item % monkey.divider == 0 {
                        monkey.when_true
                    } else {
                        monkey.when_false
                    };
                monkeys[new_monkey].items.push(new_item);
            }
            monkeys[current_monkey].items = Vec::new();
        }
    }

    inspections.sort();
    inspections.reverse();

    inspections[0] * inspections[1]
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
