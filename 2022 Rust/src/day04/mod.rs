use std::collections::HashSet;

use combine::{many1, Parser};
use combine::parser::char::{char, digit};

use crate::common::*;

fn parse(line: &str) -> (HashSet<i32>, HashSet<i32>) {
    let integer_parser = many1(digit())
        .map(|input: String| input.parse().unwrap());
    let assignment_parser = integer_parser.clone()
        .skip(char('-'))
        .and(integer_parser.clone())
        .map(|(a, b)| (a..=b).collect());
    let line_parser = assignment_parser.clone()
        .skip(char(','))
        .and(assignment_parser.clone());
    line_parser.clone().parse(line).unwrap().0
}

fn part1(input: &str) -> usize {
    input.lines()
        .filter(|line| {
            let (assignment_1, assignment_2) = parse(line);
            assignment_1.difference(&assignment_2).count() == 0
                || assignment_2.difference(&assignment_1).count() == 0
        })
        .count()
}

fn part2(input: &str) -> usize {
    input.lines()
        .filter(|line| {
            let (assignment_1, assignment_2) = parse(line);
            assignment_1.intersection(&assignment_2).count() > 0
        })
        .count()
}

#[test]
fn part1_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 1 (example): {}", part1(&read_file(&path)))
}

#[test]
fn part1_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 1 (real): {}", part1(&read_file(&path)))
}

#[test]
fn part2_example() {
    let path = file!().replace("mod.rs", "example.txt");
    println!("Part 2 (example): {}", part2(&read_file(&path)))
}

#[test]
fn part2_real() {
    let path = file!().replace("mod.rs", "input.txt");
    println!("Part 2 (real): {}", part2(&read_file(&path)))
}
