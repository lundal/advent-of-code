use std::fs;
use std::ops::Add;
use std::collections::HashSet;

#[derive(Hash, Eq, PartialEq, Debug, Copy, Clone)]
struct Point {
    x: i32,
    y: i32,
}

impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

fn trace(line: &str) -> Vec<Point> {
    let mut positions = Vec::new();
    let mut position = Point { x: 0, y: 0 };

    for segment in line.split(",") {
        let (direction, length) = segment.split_at(1);

        let unit = match direction {
            "R" => Point { x: 1, y: 0 },
            "L" => Point { x: -1, y: 0 },
            "D" => Point { x: 0, y: 1 },
            "U" => Point { x: 0, y: -1 },
            ___ => Point { x: 0, y: 0 },
        };

        let n = length.parse().expect("Not an int");

        for _ in 0..n {
            position = position + unit;
            positions.push(position);
        }
    }

    return positions;
}

fn main() {
    let filename = "input.txt";
    let contents = fs::read_to_string(filename).expect("Failed to read file");
    let lines: Vec<&str> = contents.trim().lines().collect();

    let list_a = trace(&lines[0]);
    let list_b = trace(&lines[1]);

    let set_a: HashSet<_> = list_a.iter().collect();
    let set_b: HashSet<_> = list_b.iter().collect();

    let crossings: HashSet<_> = set_a.intersection(&set_b).collect();
    println!("{:?}", crossings);

    let min_dist = crossings.iter()
        .map(|p| p.x.abs() + p.y.abs())
        .min()
        .expect("No values");

    println!("{}", min_dist);
}

