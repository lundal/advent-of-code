use std::fs;

fn main() {
    let filename = "input.txt";
    let contents = fs::read_to_string(filename).expect("Failed to read file");
    let lines = contents.lines();

    let mut total_fuel: i32 = 0;
    for line in lines {
        let mass: i32 = line.parse().expect("Not an int");
        total_fuel += fuel(mass);
    }

    println!("{}", total_fuel)
}

fn fuel(mass: i32) -> i32 {
    return mass / 3 - 2;
}
