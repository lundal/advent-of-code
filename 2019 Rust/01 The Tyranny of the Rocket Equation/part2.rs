use std::fs;

fn main() {
    let filename = "input.txt";
    let contents = fs::read_to_string(filename).expect("Failed to read file");
    let lines = contents.lines();

    let mut total_fuel: i32 = 0;
    for line in lines {
        let mass: i32 = line.parse().expect("Not an int");
        total_fuel += fuel_with_extra(mass);
    }

    println!("{}", total_fuel)
}

fn fuel(mass: i32) -> i32 {
    if mass < 6 {
        return 0;
    }
    return mass / 3 - 2;
}

fn fuel_with_extra(mass: i32) -> i32 {
    if mass == 0 {
        return 0;
    }
    let extra = fuel(mass);
    return extra + fuel_with_extra(extra);
}
