fn main() {
    let min = 109165;
    let max = 576723;
    let mut count = 0;

    for password in min..max + 1 {
        if matches(password) {
            count += 1
        }
    }
    println!("{}", count);
}

fn matches(password: i32) -> bool {
    let digits: Vec<u32> = password
        .to_string()
        .chars()
        .map(|d| d.to_digit(10).expect("Not a number"))
        .collect();

    return repeat(&digits) && !decrease(&digits);
}

fn decrease(digits: &Vec<u32>) -> bool {
    return digits.windows(2).any(|ds| ds[0] > ds[1]);
}

fn repeat(digits: &Vec<u32>) -> bool {
    return digits.windows(2).any(|ds| ds[0] == ds[1]);
}
