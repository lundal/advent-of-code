use std::fs;

fn main() {
    let filename = "input.txt";
    let contents = fs::read_to_string(filename).expect("Failed to read file");

    let memory: Vec<i32> = contents.trim()
        .split(",")
        .map(|s| s.parse().expect("Not an int"))
        .collect();

    let mut memory1 = memory.clone();

    let result = run(&mut memory1);
    println!("Result: {}", result);
    println!("Memory: {:?}", memory1);
}

fn init(memory: &Vec<i32>, noun: i32, verb: i32) -> Vec<i32> {
    let mut copy = memory.clone();
    copy[1] = noun;
    copy[2] = verb;
    return copy;
}

fn run(mut memory: &mut Vec<i32>) -> i32 {
    let mut pointer: i32 = 0;

    loop {
        let instruction = memory[pointer as usize];
        let opcode = instruction % 100;
        let mode_a = (instruction / 100) % 10;
        let mode_b = (instruction / 1000) % 10;

        // Add
        if opcode == 1 {
            let param_a = load(mode_a, &memory, pointer + 1);
            let param_b = load(mode_b, &memory, pointer + 2);
            let param_c = load_immediate(&memory, pointer + 3);
            store(&mut memory, param_c, param_a + param_b);
            pointer += 3;
        }

        // Multiply
        if opcode == 2 {
            let param_a = load(mode_a, &memory, pointer + 1);
            let param_b = load(mode_b, &memory, pointer + 2);
            let param_c = load_immediate(&memory, pointer + 3);
            store(&mut memory, param_c, param_a * param_b);
            pointer += 3;
        }

        // Input
        if opcode == 3 {
            let param_a = load_immediate(&memory, pointer + 1);
            store(&mut memory, param_a, 5); // TODO: Softcode
            pointer += 1;
        }

        // Output
        if opcode == 4 {
            let param_a = load(mode_a, &memory, pointer + 1);
            println!("{}", param_a);
            pointer += 1;
        }

        // Jump if true
        if opcode == 5 {
            let param_a = load(mode_a, &memory, pointer + 1);
            let param_b = load(mode_b, &memory, pointer + 2);
            if param_a != 0 {
                pointer = param_b - 1;
            } else {
                pointer += 2;
            }
        }

        // Jump if false
        if opcode == 6 {
            let param_a = load(mode_a, &memory, pointer + 1);
            let param_b = load(mode_b, &memory, pointer + 2);
            if param_a == 0 {
                pointer = param_b - 1;
            } else {
                pointer += 2;
            }
        }

        // Less than
        if opcode == 7 {
            let param_a = load(mode_a, &memory, pointer + 1);
            let param_b = load(mode_b, &memory, pointer + 2);
            let param_c = load_immediate(&memory, pointer + 3);
            let value = if param_a < param_b { 1 } else { 0 };
            store(&mut memory, param_c, value);
            pointer += 3;
        }

        // Equals
        if opcode == 8 {
            let param_a = load(mode_a, &memory, pointer + 1);
            let param_b = load(mode_b, &memory, pointer + 2);
            let param_c = load_immediate(&memory, pointer + 3);
            let value = if param_a == param_b { 1 } else { 0 };
            store(&mut memory, param_c, value);
            pointer += 3;
        }

        // Halt
        if opcode == 99 {
            break;
        }

        pointer += 1;
    }

    return memory[0];
}

fn load(mode: i32, memory: &Vec<i32>, position: i32) -> i32 {
    if mode == 1 {
        return load_immediate(memory, position);
    } else {
        return load_referenced(memory, position);
    }
}

fn load_immediate(memory: &Vec<i32>, position: i32) -> i32 {
    return memory[position as usize];
}

fn load_referenced(memory: &Vec<i32>, position: i32) -> i32 {
    let addr = memory[position as usize];
    return memory[addr as usize];
}

fn store(memory: &mut Vec<i32>, position: i32, value: i32) {
    return memory[position as usize] = value;
}
