use std::collections::HashMap;
use std::fs;

fn main() {
    let filename = "input.txt";
    let contents = fs::read_to_string(filename).expect("Failed to read file");

    let orbits: Vec<Vec<&str>> = contents.trim()
        .lines()
        .map(|line| line.split(")").collect())
        .collect();

    let mut hierarchy = HashMap::new();

    for orbit in orbits.iter() {
        hierarchy.insert(orbit[1], orbit[0]);
    }

    let total_steps: i32 = hierarchy.keys()
        .map(|object| steps(&hierarchy, object, "COM"))
        .sum();

    println!("{}", total_steps)
}

fn steps(hierarchy: &HashMap<&str, &str>, from: &str, to: &str) -> i32 {
    let mut steps = 0;
    let mut current = from;

    while current != to {
        steps += 1;
        current = hierarchy.get(current).expect("Impossible path");
    }

    return steps;
}
