use std::collections::HashMap;
use std::fs;

fn main() {
    let filename = "input.txt";
    let contents = fs::read_to_string(filename).expect("Failed to read file");

    let orbits: Vec<Vec<&str>> = contents.trim()
        .lines()
        .map(|line| line.split(")").collect())
        .collect();

    let mut hierarchy = HashMap::new();

    for orbit in orbits.iter() {
        hierarchy.insert(orbit[1], orbit[0]);
    }

    let mut my_steps = steps(&hierarchy, "YOU", "COM");
    let mut santa_steps = steps(&hierarchy, "SAN", "COM");

    while my_steps.last() == santa_steps.last() {
        my_steps.pop();
        santa_steps.pop();
    }

    let transfers = my_steps.len() + santa_steps.len();

    println!("{}", transfers);
}

fn steps<'a>(hierarchy: &HashMap<&str, &'a str>, from: &str, to: &str) -> Vec<&'a str> {
    let mut steps = Vec::new();
    let mut current = from;

    while current != to {
        let next = hierarchy.get(current).expect("Impossible path").clone();
        steps.push(next);
        current = next;
    }

    return steps;
}
