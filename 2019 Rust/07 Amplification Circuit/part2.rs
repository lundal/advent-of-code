use std::fs;
use std::sync::mpsc::channel;
use std::sync::mpsc::Receiver;
use std::sync::mpsc::Sender;
use std::thread;

fn main() {
    let filename = "input.txt";
    let contents = fs::read_to_string(filename).expect("Failed to read file");

    let memory: Vec<i32> = contents.trim()
        .split(",")
        .map(|s| s.parse().expect("Not an int"))
        .collect();

    let mut best_phase = vec!(0, 0, 0, 0);
    let mut best_res = 0;

    for a in 5..10 {
        for b in 5..10 {
            if b == a {
                continue;
            }
            for c in 5..10 {
                if c == b || c == a {
                    continue;
                }
                for d in 5..10 {
                    if d == c || d == b || d == a {
                        continue;
                    }
                    for e in 5..10 {
                        if e == d || e == c || e == b || e == a {
                            continue;
                        }
                        let phases = vec!(a, b, c, d, e);
                        let res = run_pipeline(&memory, &phases);

                        if res > best_res {
                            best_phase = phases;
                            best_res = res;
                        }
                    }
                }
            }
        }
    }

    println!("{:?}", best_phase);
    println!("{}", best_res);
}

fn run_pipeline(memory: &Vec<i32>, phases: &Vec<i32>) -> i32 {
    let (tx_a, rx_a) = channel();
    let (tx_b, rx_b) = channel();
    let (tx_c, rx_c) = channel();
    let (tx_d, rx_d) = channel();
    let (tx_e, rx_e) = channel();

    // Init
    tx_a.send(phases[0]).expect("Phase a failed");
    tx_b.send(phases[1]).expect("Phase b failed");
    tx_c.send(phases[2]).expect("Phase c failed");
    tx_d.send(phases[3]).expect("Phase d failed");
    tx_e.send(phases[4]).expect("Phase e failed");

    // Zero
    tx_a.send(0).expect("Init failed");

    let mut mem_a = memory.clone();
    let mut mem_b = memory.clone();
    let mut mem_c = memory.clone();
    let mut mem_d = memory.clone();
    let mut mem_e = memory.clone();

    let t_a = thread::spawn(move || {
        run(&mut mem_a, &rx_a, &tx_b);
        return rx_a;
    });
    let t_b = thread::spawn(move || run(&mut mem_b, &rx_b, &tx_c));
    let t_c = thread::spawn(move || run(&mut mem_c, &rx_c, &tx_d));
    let t_d = thread::spawn(move || run(&mut mem_d, &rx_d, &tx_e));
    let t_e = thread::spawn(move || run(&mut mem_e, &rx_e, &tx_a));

    let rx_t = t_a.join().expect("Thread a failed");
    t_b.join().expect("Thread b failed");
    t_c.join().expect("Thread c failed");
    t_d.join().expect("Thread d failed");
    t_e.join().expect("Thread e failed");

    return rx_t.recv().expect("No output");
}

fn run(mut memory: &mut Vec<i32>, input: &Receiver<i32>, output: &Sender<i32>) -> i32 {
    let mut pointer: i32 = 0;

    loop {
        let instruction = memory[pointer as usize];
        let opcode = instruction % 100;
        let mode_a = (instruction / 100) % 10;
        let mode_b = (instruction / 1000) % 10;

        // Add
        if opcode == 1 {
            let param_a = load(mode_a, &memory, pointer + 1);
            let param_b = load(mode_b, &memory, pointer + 2);
            let param_c = load_immediate(&memory, pointer + 3);
            store(&mut memory, param_c, param_a + param_b);
            pointer += 3;
        }

        // Multiply
        if opcode == 2 {
            let param_a = load(mode_a, &memory, pointer + 1);
            let param_b = load(mode_b, &memory, pointer + 2);
            let param_c = load_immediate(&memory, pointer + 3);
            store(&mut memory, param_c, param_a * param_b);
            pointer += 3;
        }

        // Input
        if opcode == 3 {
            let param_a = load_immediate(&memory, pointer + 1);
            store(&mut memory, param_a, input.recv().expect("No input!"));
            pointer += 1;
        }

        // Output
        if opcode == 4 {
            let param_a = load(mode_a, &memory, pointer + 1);
            output.send(param_a).expect("No output!");
            pointer += 1;
        }

        // Jump if true
        if opcode == 5 {
            let param_a = load(mode_a, &memory, pointer + 1);
            let param_b = load(mode_b, &memory, pointer + 2);
            if param_a != 0 {
                pointer = param_b - 1;
            } else {
                pointer += 2;
            }
        }

        // Jump if false
        if opcode == 6 {
            let param_a = load(mode_a, &memory, pointer + 1);
            let param_b = load(mode_b, &memory, pointer + 2);
            if param_a == 0 {
                pointer = param_b - 1;
            } else {
                pointer += 2;
            }
        }

        // Less than
        if opcode == 7 {
            let param_a = load(mode_a, &memory, pointer + 1);
            let param_b = load(mode_b, &memory, pointer + 2);
            let param_c = load_immediate(&memory, pointer + 3);
            let value = if param_a < param_b { 1 } else { 0 };
            store(&mut memory, param_c, value);
            pointer += 3;
        }

        // Equals
        if opcode == 8 {
            let param_a = load(mode_a, &memory, pointer + 1);
            let param_b = load(mode_b, &memory, pointer + 2);
            let param_c = load_immediate(&memory, pointer + 3);
            let value = if param_a == param_b { 1 } else { 0 };
            store(&mut memory, param_c, value);
            pointer += 3;
        }

        // Halt
        if opcode == 99 {
            break;
        }

        pointer += 1;
    }

    return memory[0];
}

fn load(mode: i32, memory: &Vec<i32>, position: i32) -> i32 {
    if mode == 1 {
        return load_immediate(memory, position);
    } else {
        return load_referenced(memory, position);
    }
}

fn load_immediate(memory: &Vec<i32>, position: i32) -> i32 {
    return memory[position as usize];
}

fn load_referenced(memory: &Vec<i32>, position: i32) -> i32 {
    let addr = memory[position as usize];
    return memory[addr as usize];
}

fn store(memory: &mut Vec<i32>, position: i32, value: i32) {
    return memory[position as usize] = value;
}
