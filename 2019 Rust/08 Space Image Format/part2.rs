use std::fs;

fn main() {
    let filename = "input.txt";
    let contents = fs::read_to_string(filename).expect("Failed to read file");

    let digits: Vec<u32> = contents.trim()
        .chars()
        .map(|c| c.to_digit(10).expect("Not an int"))
        .collect();

    let width: usize = 25;
    let height: usize = 6;

    let image: Vec<Vec<Vec<u32>>> = digits.chunks(width * height)
        .map(|layer| layer.to_vec().chunks(width)
            .map(|row| row.to_vec())
            .collect())
        .collect();

    for y in 0..height {
        for x in 0..width {
            let color = top_color(&image, y, x);
            if color == 1 {
                print!("XX")
            } else {
                print!("  ")
            }
        }
        println!()
    }
}

fn top_color(image: &Vec<Vec<Vec<u32>>>, y: usize, x: usize) -> u32 {
    return image.iter()
        .map(|layer: &Vec<Vec<u32>>| layer[y][x])
        .find(|color: &u32| *color != 2)
        .expect("No color!");
}
