use std::fs;

fn main() {
    let filename = "input.txt";
    let contents = fs::read_to_string(filename).expect("Failed to read file");

    let digits: Vec<u32> = contents.trim()
        .chars()
        .map(|c| c.to_digit(10).expect("Not an int"))
        .collect();

    let width: usize = 25;
    let height: usize = 6;

    let image: Vec<Vec<Vec<u32>>> = digits.chunks(width * height)
        .map(|layer| layer.to_vec().chunks(width)
            .map(|row| row.to_vec())
            .collect())
        .collect();

    let mut min_zeroes = 9999;
    let mut checksum = 9999;

    for layer in image.iter() {
        let zeroes = count(layer, 0);
        let ones = count(layer, 1);
        let twos = count(layer, 2);

        if zeroes < min_zeroes {
            min_zeroes = zeroes;
            checksum = ones * twos;
        }
    }

    println!("{}", checksum);
}

fn count(layer: &Vec<Vec<u32>>, value: u32) -> usize {
    return layer.iter()
        .map(|row: &Vec<u32>| row.iter()
            .filter(|cell: &&u32| **cell == value)
            .count())
        .sum();
}
