use std::fs;

fn main() {
    let filename = "input.txt";
    let contents = fs::read_to_string(filename).expect("Failed to read file");

    let memory: Vec<i32> = contents.trim()
        .split(",")
        .map(|s| s.parse().expect("Not an int"))
        .collect();

    for noun in 0..100 {
        for verb in 0..100 {
            let result = run(init(&memory, noun, verb));
            if result == 19690720 {
                println!("{} {}", noun, verb);
            }
        }
    }
}

fn init(memory: &Vec<i32>, noun: i32, verb: i32) -> Vec<i32> {
    let mut copy = memory.clone();
    copy[1] = noun;
    copy[2] = verb;
    return copy;
}

fn run(mut memory: Vec<i32>) -> i32 {
    let mut pointer = 0;

    loop {
        let opcode = memory[pointer];

        if opcode == 1 {
            let addr_a = memory[pointer + 1] as usize;
            let addr_b = memory[pointer + 2] as usize;
            let addr_c = memory[pointer + 3] as usize;
            memory[addr_c] = memory[addr_a] + memory[addr_b];
            pointer += 3;
        }

        if opcode == 2 {
            let addr_a = memory[pointer + 1] as usize;
            let addr_b = memory[pointer + 2] as usize;
            let addr_c = memory[pointer + 3] as usize;
            memory[addr_c] = memory[addr_a] * memory[addr_b];
            pointer += 3;
        }

        if opcode == 99 {
            break;
        }

        pointer += 1;
    }

    return memory[0];
}
