use std::fs;

fn main() {
    let filename = "input.txt";
    let contents = fs::read_to_string(filename).expect("Failed to read file");

    let mut memory: Vec<i32> = contents.trim()
        .split(",")
        .map(|s| s.parse().expect("Not an int"))
        .collect();

    let mut ip = 0;

    loop {
        let opcode = memory[ip];

        if opcode == 1 {
            let addr_a = memory[ip + 1];
            let addr_b = memory[ip + 2];
            let addr_c = memory[ip + 3];
            memory[addr_c as usize] = memory[addr_a as usize] + memory[addr_b as usize];
            ip += 3;
        }

        if opcode == 2 {
            let addr_a  = memory[ip + 1];
            let addr_b = memory[ip + 2];
            let addr_c = memory[ip + 3];
            memory[addr_c as usize] = memory[addr_a as usize] * memory[addr_b as usize];
            ip += 3;
        }

        if opcode == 99 {
            break;
        }

        ip += 1;
    }

    println!("{:?}", memory)
}

