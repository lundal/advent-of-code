use std::fs;
use std::sync::mpsc::channel;
use std::sync::mpsc::Receiver;
use std::sync::mpsc::Sender;

fn main() {
    let filename = "input.txt";
    let contents = fs::read_to_string(filename).expect("Failed to read file");

    let mut memory: Vec<i64> = contents.trim()
        .split(",")
        .map(|s| s.parse().expect("Not an int"))
        .collect();

    memory.resize(2000, 0);

    let (tx_input, rx_input) = channel();
    let (tx_output, rx_output) = channel();

    tx_input.send(2).expect("Input failed");

    run(&mut memory.clone(), &rx_input, &tx_output);

    drop(tx_input);
    drop(tx_output);

    for output in rx_output {
        println!("{}", output);
    }
}

fn run(mut memory: &mut Vec<i64>, input: &Receiver<i64>, output: &Sender<i64>) -> i64 {
    let mut instruction_counter: i64 = 0;
    let mut relative_base: i64 = 0;

    loop {
        let instruction = memory[instruction_counter as usize];
        let opcode = instruction % 100;
        let mode_a = (instruction / 100) % 10;
        let mode_b = (instruction / 1000) % 10;
        let mode_c = (instruction / 10000) % 10;

        // Add
        if opcode == 1 {
            let param_a = load(mode_a, &memory, relative_base, instruction_counter, 1);
            let param_b = load(mode_b, &memory, relative_base, instruction_counter, 2);
            let param_c = load_immediate(&memory, instruction_counter + 3);
            if mode_c == 2 {
                store(&mut memory, relative_base + param_c, param_a + param_b);
            } else {
                store(&mut memory, param_c, param_a + param_b);
            }
            instruction_counter += 3;
        }

        // Multiply
        if opcode == 2 {
            let param_a = load(mode_a, &memory, relative_base, instruction_counter, 1);
            let param_b = load(mode_b, &memory, relative_base, instruction_counter, 2);
            let param_c = load_immediate(&memory, instruction_counter + 3);
            if mode_c == 2 {
                store(&mut memory, relative_base + param_c, param_a * param_b);
            } else {
                store(&mut memory, param_c, param_a * param_b);
            }
            instruction_counter += 3;
        }

        // Input
        if opcode == 3 {
            let param_a = load_immediate(&memory, instruction_counter + 1);
            if mode_a == 2 {
                store(&mut memory, relative_base + param_a, input.recv().expect("No input!"));
            } else {
                store(&mut memory, param_a, input.recv().expect("No input!"));
            }
            instruction_counter += 1;
        }

        // Output
        if opcode == 4 {
            let param_a = load(mode_a, &memory, relative_base, instruction_counter, 1);
            output.send(param_a).expect("No output!");
            instruction_counter += 1;
        }

        // Jump if true
        if opcode == 5 {
            let param_a = load(mode_a, &memory, relative_base, instruction_counter, 1);
            let param_b = load(mode_b, &memory, relative_base, instruction_counter, 2);
            if param_a != 0 {
                instruction_counter = param_b - 1;
            } else {
                instruction_counter += 2;
            }
        }

        // Jump if false
        if opcode == 6 {
            let param_a = load(mode_a, &memory, relative_base, instruction_counter, 1);
            let param_b = load(mode_b, &memory, relative_base, instruction_counter, 2);
            if param_a == 0 {
                instruction_counter = param_b - 1;
            } else {
                instruction_counter += 2;
            }
        }

        // Less than
        if opcode == 7 {
            let param_a = load(mode_a, &memory, relative_base, instruction_counter, 1);
            let param_b = load(mode_b, &memory, relative_base, instruction_counter, 2);
            let param_c = load_immediate(&memory, instruction_counter + 3);
            let value = if param_a < param_b { 1 } else { 0 };
            if mode_c == 2 {
                store(&mut memory, relative_base + param_c, value);
            } else {
                store(&mut memory, param_c, value);
            }
            instruction_counter += 3;
        }

        // Equals
        if opcode == 8 {
            let param_a = load(mode_a, &memory, relative_base, instruction_counter, 1);
            let param_b = load(mode_b, &memory, relative_base, instruction_counter, 2);
            let param_c = load_immediate(&memory, instruction_counter + 3);
            let value = if param_a == param_b { 1 } else { 0 };
            if mode_c == 2 {
                store(&mut memory, relative_base + param_c, value);
            } else {
                store(&mut memory, param_c, value);
            }
            instruction_counter += 3;
        }

        // Adjust relative base
        if opcode == 9 {
            let param_a = load(mode_a, &memory, relative_base, instruction_counter, 1);
            relative_base += param_a;
            instruction_counter += 1;
        }

        // Halt
        if opcode == 99 {
            break;
        }

        instruction_counter += 1;
    }

    return memory[0];
}

fn load(mode: i64, memory: &Vec<i64>, relative_base: i64, instruction_counter: i64, offset: i64) -> i64 {
    if mode == 2 {
        let relative_offset = load_immediate(memory, instruction_counter + offset);
        return load_immediate(memory, relative_base + relative_offset);
    } else if mode == 1 {
        return load_immediate(memory, instruction_counter + offset);
    } else {
        let reference = load_immediate(memory, instruction_counter + offset);
        return load_immediate(memory, reference);
    }
}

fn load_immediate(memory: &Vec<i64>, position: i64) -> i64 {
    return memory[position as usize];
}

fn store(memory: &mut Vec<i64>, position: i64, value: i64) {
    return memory[position as usize] = value;
}
