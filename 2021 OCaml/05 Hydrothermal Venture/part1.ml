#load "str.cma";;

let rec read_lines (file: in_channel): string list =
    try
        let line = Stdlib.input_line file in
        line :: read_lines file
    with Stdlib.End_of_file ->
        Stdlib.close_in file;
        [];;

type point = { x: int; y: int; };;
type vector = point * point;;

let parse_vector (line: string): vector =
    let x1::y1::x2::y2::[] = Str.split (Str.regexp "\\( -> \\|,\\)") line in
    (
        { x = Stdlib.int_of_string x1; y = Stdlib.int_of_string y1; },
        { x = Stdlib.int_of_string x2; y = Stdlib.int_of_string y2; }
    )

let to_points ((a, b): vector): point list =
    let points = ref [] in
    for x = Stdlib.min a.x b.x to Stdlib.max a.x b.x do
        for y = Stdlib.min a.y  b.y to Stdlib.max a.y b.y do
            points := { x = x; y = y; } :: !points;
        done;
    done;
    !points;;

let is_horizontal_or_vertical (vec: vector): bool =
    let (a, b) = vec in
    a.x = b.x || a.y = b.y;;

module Point =
    struct
        type t = point
        let compare a b =
            match Stdlib.compare a.x b.x with
            | 0 -> Stdlib.compare a.y b.y
            | c -> c
    end;;

module PointMap = Map.Make(Point);;

let count_distinct (list: point list) =
    let increment current =
        match current with
        | Some(n) -> Some(n+1)
        | _ -> Some(1) in
    let count map point =
        PointMap.update point increment map in
    List.fold_left count PointMap.empty list;;

let points: point list =
    read_lines (Stdlib.open_in "input.txt")
    |> List.map parse_vector
    |> List.filter is_horizontal_or_vertical
    |> List.map to_points
    |> List.flatten;;

let crossings: int =
    count_distinct points
    |> PointMap.filter (fun k v -> v > 1)
    |> PointMap.bindings
    |> List.length;;

Format.printf "Crossings: %d\n" crossings;;
