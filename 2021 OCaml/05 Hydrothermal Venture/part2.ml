#load "str.cma";;

let rec read_lines (file: in_channel): string list =
    try
        let line = Stdlib.input_line file in
        line :: read_lines file
    with Stdlib.End_of_file ->
        Stdlib.close_in file;
        [];;

type point = { x: int; y: int; };;
type vector = point * point;;

let parse_vector (line: string): vector =
    let x1::y1::x2::y2::[] = Str.split (Str.regexp "\\( -> \\|,\\)") line in
    (
        { x = Stdlib.int_of_string x1; y = Stdlib.int_of_string y1; },
        { x = Stdlib.int_of_string x2; y = Stdlib.int_of_string y2; }
    )

let to_points ((a, b): vector): point list =
    let points = ref [] in
    let dx = b.x - a.x in
    let dy = b.y - a.y in
    let len = Stdlib.max (Stdlib.abs dx) (Stdlib.abs dy) in
    for i = 0 to len do
        let x = a.x + dx * i / len in
        let y = a.y + dy * i / len in
        points := { x = x; y = y; } :: !points;
    done;
    !points;;

module Point =
    struct
        type t = point
        let compare a b =
            match Stdlib.compare a.x b.x with
            | 0 -> Stdlib.compare a.y b.y
            | c -> c
    end;;

module PointMap = Map.Make(Point);;

let count_distinct (list: point list) =
    let increment current =
        match current with
        | Some(n) -> Some(n+1)
        | _ -> Some(1) in
    let count map point =
        PointMap.update point increment map in
    List.fold_left count PointMap.empty list;;

let points: point list =
    read_lines (Stdlib.open_in "input.txt")
    |> List.map parse_vector
    |> List.map to_points
    |> List.flatten;;

let crossings: int =
    count_distinct points
    |> PointMap.filter (fun k v -> v > 1)
    |> PointMap.bindings
    |> List.length;;

Format.printf "Crossings: %d\n" crossings;;
