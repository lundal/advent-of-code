let read_lines (filename: string): string list =
    let lines = ref [] in
    let file = Stdlib.open_in filename in
    try
        while true do
            let line = Stdlib.input_line file in
            lines := line :: !lines
        done;
        List.rev !lines
    with Stdlib.End_of_file ->
        Stdlib.close_in file;
        List.rev !lines;;

let drop_first (list: 'a list): 'a list =
    List.tl list;;

let drop_last (list: 'a list): 'a list =
    List.rev (List.tl (List.rev list));;

let rec sliding_window_3 (list: int list): int list =
    match list with
    | h1::h2::h3::rest -> (h1 + h2 + h3) :: sliding_window_3 (h2::h3::rest)
    | _ -> [];;

let numbers: int list =
    read_lines "input.txt"
    |> List.map Stdlib.int_of_string
    |> sliding_window_3;;

let count: int =
    List.map2 (fun a b -> b - a) (drop_last numbers) (drop_first numbers)
    |> List.filter (fun a -> a > 0)
    |> List.length;;

Stdlib.print_int count;;
