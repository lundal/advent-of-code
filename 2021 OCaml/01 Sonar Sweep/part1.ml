let read_lines (filename: string): string list =
    let lines = ref [] in
    let file = Stdlib.open_in filename in
    try
        while true do
            let line = Stdlib.input_line file in
            lines := line :: !lines
        done;
        List.rev !lines
    with Stdlib.End_of_file ->
        Stdlib.close_in file;
        List.rev !lines;;

let drop_first (list: 'a list): 'a list =
    List.tl list;;

let drop_last (list: 'a list): 'a list =
    List.rev (List.tl (List.rev list));;

let numbers: int list =
    read_lines "input.txt"
    |> List.map Stdlib.int_of_string;;

let count: int =
    List.map2 (fun a b -> b - a) (drop_last numbers) (drop_first numbers)
    |> List.filter (fun a -> a > 0)
    |> List.length;;

Stdlib.print_int count;;
