let rec read_lines (file: in_channel): string list =
    try
        let line = Stdlib.input_line file in
        line :: read_lines file
    with Stdlib.End_of_file ->
        Stdlib.close_in file;
        [];;

type command =
    | Forward of int
    | Down of int
    | Up of int;;

type position =
    { depth: int;
      horizontal: int;
      aim: int;
    };;

let parse_command (text: string): command option =
    match String.split_on_char ' ' text with
    | "forward"::n::[] -> Some (Forward (Stdlib.int_of_string n))
    | "down"::n::[] -> Some (Down (Stdlib.int_of_string n))
    | "up"::n::[] -> Some (Up (Stdlib.int_of_string n))
    | _ -> None;;

let execute_command (pos: position) (cmd: command): position =
    match cmd with
    | Forward n -> { depth = pos.depth + pos.aim * n; horizontal = pos.horizontal + n; aim = pos.aim}
    | Down n -> { depth = pos.depth; horizontal = pos.horizontal; aim = pos.aim + n }
    | Up n -> { depth = pos.depth; horizontal = pos.horizontal; aim = pos.aim - n };;

let pos: position =
    read_lines (Stdlib.open_in "input.txt")
    |> List.filter_map parse_command
    |> List.fold_left execute_command { depth = 0; horizontal = 0; aim = 0 };;

Format.printf "Depth: %d, Horizontal: %d, Product: %d\n"
    pos.depth pos.horizontal (pos.depth * pos.horizontal)
