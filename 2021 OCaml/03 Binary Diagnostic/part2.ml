let rec read_lines (file: in_channel): string list =
    try
        let line = Stdlib.input_line file in
        line :: read_lines file
    with Stdlib.End_of_file ->
        Stdlib.close_in file;
        [];;

let rec filter_oxygen (i: int) (lines: string list): string =
    match lines with
    | line::[] -> line
    | _ ->
        let is_zero line = String.get line i = '0' in
        let (zeroes, ones) = List.partition is_zero lines in
        if List.length ones >= List.length zeroes then
            filter_oxygen (i+1) ones
        else
            filter_oxygen (i+1) zeroes
        ;;

let rec filter_co2 (i: int) (lines: string list): string =
    match lines with
    | line::[] -> line
    | _ ->
        let is_zero line = String.get line i = '0' in
        let (zeroes, ones) = List.partition is_zero lines in
        if List.length ones >= List.length zeroes then
            filter_co2 (i+1) zeroes
        else
            filter_co2 (i+1) ones
        ;;

let to_number (binary: string): int =
    Stdlib.int_of_string ("0b" ^ binary);;

let lines: string list =
    read_lines (Stdlib.open_in "input.txt");;

let oxygen = filter_oxygen 0 lines |> to_number in
let co2 = filter_co2 0 lines |> to_number in
Format.printf "Oxygen: %d, CO2: %d, Product: %d\n"
    oxygen co2 (oxygen * co2)
