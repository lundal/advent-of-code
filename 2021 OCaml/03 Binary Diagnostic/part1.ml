let rec read_lines (file: in_channel): string list =
    try
        let line = Stdlib.input_line file in
        line :: read_lines file
    with Stdlib.End_of_file ->
        Stdlib.close_in file;
        [];;

type state =
    { zeroes: int;
      ones: int;
    };;

let to_chars (text: string): char list =
    List.init (String.length text) (String.get text);;

let to_state (c: char): state option =
    match c with
    | '0' -> Some { zeroes = 1; ones = 0 }
    | '1' -> Some { zeroes = 0; ones = 1 }
    | _ -> None;;

let combine_states (a: state) (b: state): state =
    { zeroes = a.zeroes + b.zeroes;
      ones = a.ones + b.ones;
    };;

let parse_line (text: string): state list =
    to_chars text
    |> List.filter_map to_state;;

let combine_lines (a: state list) (b: state list): state list =
    List.map2 combine_states a b;;

let rec map_combine (map: 'a -> 'b) (combine: 'b -> 'b -> 'b) (l: 'a list): 'b =
    match l with
    | head::[] -> map head
    | head::tail -> combine (map head) (map_combine map combine tail)
    | _ ->  Stdlib.failwith "empty"

let to_gamma (s: state): string =
    if s.ones > s.zeroes then "1" else "0";;

let to_epsilon (s: state): string =
    if s.ones < s.zeroes then "1" else "0";;

let to_number (map: state -> string) (states: state list): int =
    let binary = states
        |> List.map map
        |> List.fold_left (^) "" in
    Stdlib.int_of_string ("0b" ^ binary);;

let states: state list =
    read_lines (Stdlib.open_in "input.txt")
    |> map_combine parse_line combine_lines;;

let gamma = to_number to_gamma states in
let epsilon = to_number to_epsilon states in
Format.printf "Gamma: %d, Epsilon: %d, Product: %d\n"
    gamma epsilon (gamma * epsilon)
