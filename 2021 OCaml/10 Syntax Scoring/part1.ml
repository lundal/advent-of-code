open Core;;
open Stdio;;

type result =
    | Unexpected_char of char
    | Unexpected_end
    | Ok;;

let rec validate_block (blocks: char list) (tokens: char list): result =
    match (blocks, tokens) with
    | (_, '('::ts) -> validate_block ('('::blocks) ts
    | (_, '['::ts) -> validate_block ('['::blocks) ts
    | (_, '{'::ts) -> validate_block ('{'::blocks) ts
    | (_, '<'::ts) -> validate_block ('<'::blocks) ts
    | ('('::bs, ')'::ts) -> validate_block bs ts
    | ('['::bs, ']'::ts) -> validate_block bs ts
    | ('{'::bs, '}'::ts) -> validate_block bs ts
    | ('<'::bs, '>'::ts) -> validate_block bs ts
    | (_, t::_) -> Unexpected_char t
    | (_::_, []) -> Unexpected_end
    | ([], []) -> Ok;;

let validate_line (line: string): result =
    let tokens = String.to_list line in
    validate_block [] tokens;;

let to_score (res: result): int =
    match res with
    | Unexpected_char ')' -> 3
    | Unexpected_char ']' -> 57
    | Unexpected_char '}' -> 1197
    | Unexpected_char '>' -> 25137
    | _ -> 0;;

let score: int =
    "input.txt"
        |> In_channel.with_file ~f:In_channel.input_lines
        |> List.map ~f:validate_line
        |> List.map ~f:to_score
        |> List.fold ~init:0 ~f:(+);;

printf "Score: %d\n" score;;
