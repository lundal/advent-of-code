open Core;;
open Stdio;;

type result =
    | Unexpected_char of char
    | Unexpected_end of char list
    | Ok;;

let rec validate_block (blocks: char list) (tokens: char list): result =
    match (blocks, tokens) with
    | (_, '('::ts) -> validate_block ('('::blocks) ts
    | (_, '['::ts) -> validate_block ('['::blocks) ts
    | (_, '{'::ts) -> validate_block ('{'::blocks) ts
    | (_, '<'::ts) -> validate_block ('<'::blocks) ts
    | ('('::bs, ')'::ts) -> validate_block bs ts
    | ('['::bs, ']'::ts) -> validate_block bs ts
    | ('{'::bs, '}'::ts) -> validate_block bs ts
    | ('<'::bs, '>'::ts) -> validate_block bs ts
    | (_, t::_) -> Unexpected_char t
    | (_::_, []) -> Unexpected_end blocks
    | ([], []) -> Ok;;

let validate_line (line: string): result =
    let tokens = String.to_list line in
    validate_block [] tokens;;

let score_block (block: char): int =
    match block with
    | '(' -> 1
    | '[' -> 2
    | '{' -> 3
    | '<' -> 4
    | _ -> 0;;

let to_score (res: result): int =
    match res with
    | Unexpected_end blocks ->
        List.fold ~init:0 ~f:(fun score block -> score * 5 + score_block block) blocks
    | _ -> 0;;

let middle (l: 'a list): 'a =
    List.nth_exn l (List.length l / 2);;

let score: int =
    "input.txt"
        |> In_channel.with_file ~f:In_channel.input_lines
        |> List.map ~f:validate_line
        |> List.map ~f:to_score
        |> List.filter ~f:(fun score -> score > 0)
        |> List.sort ~compare:Int.compare
        |> middle;;

printf "Score: %d\n" score;;
