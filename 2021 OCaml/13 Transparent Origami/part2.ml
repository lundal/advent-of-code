open Core;;
open Stdio;;

type point = int * int;;

module Point = struct
    module T = struct
        type t = point
        let compare = Tuple2.compare ~cmp1:Int.compare ~cmp2:Int.compare
        let sexp_of_t = Tuple2.sexp_of_t Int.sexp_of_t Int.sexp_of_t
        let t_of_sexp = Tuple2.t_of_sexp Int.t_of_sexp Int.t_of_sexp
    end
    include T
    include Comparable.Make(T)
end

type paper = (Point.t, Point.comparator_witness) Set.t;;

type instruction =
    | Point of point
    | Fold_Up of int
    | Fold_Left of int;;

let parse_instruction (line: string): instruction option =
    if String.contains line 'x' then
        match String.split ~on:'=' line with
        | _::x::[] -> Some(Fold_Left (Int.of_string x))
        | _ -> None
    else if String.contains line 'y' then
        match String.split ~on:'=' line with
        | _::y::[] -> Some(Fold_Up (Int.of_string y))
        | _ -> None
    else
        match String.split ~on:',' line with
        | x::y::[] -> Some(Point (Int.of_string x, Int.of_string y))
        | _ -> None;;

let rec fold_paper (paper: paper) (instructions: instruction list): paper =
    match instructions with
    | (Point p)::is -> fold_paper (Set.add paper p) is
    | (Fold_Up y_fold)::is ->
        let rec fold_up (paper: paper) (points: point list): paper =
            match points with
            | (x,y)::ps -> fold_up (Set.add paper (x, (if y > y_fold then 2*y_fold - y else y))) ps
            | [] -> paper
        in fold_paper (fold_up (Set.empty (module Point)) (Set.to_list paper)) is
    | (Fold_Left x_fold)::is ->
        let rec fold_left (paper: paper) (points: point list): paper =
            match points with
            | (x,y)::ps -> fold_left (Set.add paper ((if x > x_fold then 2*x_fold - x else x), y)) ps
            | [] -> paper
        in fold_paper (fold_left (Set.empty (module Point)) (Set.to_list paper)) is
    | [] -> paper;;

let paper: paper =
    "input.txt"
        |> In_channel.with_file ~f:In_channel.input_lines
        |> List.filter_map ~f:parse_instruction
        |> fold_paper (Set.empty (module Point));;

for y = 0 to 5 do
    for x = 0 to 50 do
        match Set.mem paper (x,y) with
        | true -> printf "##"
        | false -> printf "  "
    done;
    printf "\n"
done;
