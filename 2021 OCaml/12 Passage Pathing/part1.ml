open Core;;
open Stdio;;

let not_in (item: string) (list: string list): bool =
    match List.find ~f:(String.equal item) list with
    | Some(_) -> false
    | None -> true;;

let is_uppercase (s: string): bool =
    String.equal s (String.uppercase s);;

type graph = (string, string list, String.comparator_witness) Map.t;;
type edge = string * string

let parse_line (line: string): edge =
    match String.split ~on:'-' line with
    | a::b::[] -> (a, b)
    | _ -> failwith "Unexpected input";;

let empty_graph: graph =
    Map.empty (module String);;

let rec build_graph (graph: graph) (edges: edge list): graph =
    match edges with
    | [] -> graph
    | (a,b)::tail ->
        let g = graph
            |> Map.add_multi ~key:a ~data:b
            |> Map.add_multi ~key:b ~data:a
        in build_graph g tail;;

let rec find_paths (path: string list) (graph: graph) : string list list =
    match path with
    | [] -> failwith "Unexpected path"
    | "end"::_ -> [ path ]
    | node::_ ->
        Map.find_multi graph node
            |> List.filter ~f:(fun n -> is_uppercase n || not_in n path)
            |> List.concat_map ~f:(fun n -> find_paths (n::path) graph);;

let paths: string list list =
    "input.txt"
        |> In_channel.with_file ~f:In_channel.input_lines
        |> List.map ~f:parse_line
        |> build_graph empty_graph
        |> find_paths ["start"];;

printf "Paths: %d\n" (List.length paths);;
