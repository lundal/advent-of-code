let rec read_lines (file: in_channel): string list =
    try
        let line = Stdlib.input_line file in
        line :: read_lines file
    with Stdlib.End_of_file ->
        Stdlib.close_in file;
        [];;

type entry =
    { patterns: string list;
      outputs: string list;
    };;

let parse_entry (line: string): entry =
    let patterns::outputs::[] = String.split_on_char '|' line in
    { patterns = String.split_on_char ' ' patterns;
      outputs = String.split_on_char ' ' outputs;
    };;

let is_one (value: string): bool =
    String.length value = 2;;

let is_four (value: string): bool =
    String.length value = 4;;

let is_seven (value: string): bool =
    String.length value = 3;;

let is_eight (value: string): bool =
    String.length value = 7;;

let is_simple_output (value: string): bool =
    is_one value || is_four value || is_seven value || is_eight value;;

let count_simple_outputs (entry: entry): int =
    entry.outputs
        |> List.filter is_simple_output
        |> List.length;;

let count = read_lines (Stdlib.open_in "input.txt")
    |> List.map parse_entry
    |> List.map count_simple_outputs
    |> List.fold_left (+) 0;;

Format.printf "Count: %d\n" count;;
