let rec read_lines (file: in_channel): string list =
    try
        let line = Stdlib.input_line file in
        line :: read_lines file
    with Stdlib.End_of_file ->
        Stdlib.close_in file;
        [];;

let not_empty (text: string): bool =
    String.length text > 0;;

let to_chars (text: string): char list =
    List.init (String.length text) (String.get text);;

let contains_all (pattern: string) (value: string): bool =
    List.for_all (String.contains value) (to_chars pattern);;

let contains_n (n: int) (pattern: string) (value: string): bool =
    List.filter (String.contains value) (to_chars pattern)
        |> List.length = n;;

let join_digits (digits: int list): int =
    digits
        |> List.map Stdlib.string_of_int
        |> String.concat ""
        |> Stdlib.int_of_string;;

type entry =
    { patterns: string list;
      outputs: string list;
    };;

let parse_entry (line: string): entry =
    let patterns::outputs::[] = String.split_on_char '|' line in
    { patterns = String.split_on_char ' ' patterns |> List.filter not_empty;
      outputs = String.split_on_char ' ' outputs |> List.filter not_empty;
    };;

let find_digit (patterns: string list) (value: string): int =
    let one_pattern::[] = List.filter (fun p -> String.length p = 2) patterns in
    let four_pattern::[] = List.filter (fun p -> String.length p = 4) patterns in
    let seven_pattern::[] = List.filter (fun p -> String.length p = 3) patterns in
    match String.length value with
    | 2 -> 1
    | 3 -> 7
    | 4 -> 4
    | 5 ->
        if contains_all one_pattern value then 3
        else if contains_n 3 four_pattern value then 5
        else 2
    | 6 ->
        if contains_all four_pattern value then 9
        else if contains_all seven_pattern value then 0
        else 6
    | 7 -> 8
    | _ -> Stdlib.failwith ("unexpected value: " ^ value);;

let decode_value (entry: entry): int =
    entry.outputs
        |> List.map (find_digit entry.patterns)
        |> join_digits;;

let count = read_lines (Stdlib.open_in "input.txt")
    |> List.map parse_entry
    |> List.map decode_value
    |> List.fold_left (+) 0;;

Format.printf "Count: %d\n" count;;
