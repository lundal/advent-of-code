open Core;;
open Stdio;;

type row = int array;;
type matrix = int array array;;

let parse_row (line: string): row =
    line
        |> String.to_list
        |> List.map ~f:Char.to_string
        |> List.map ~f:Int.of_string
        |> List.to_array;;

let parse_matrix (lines: string list): matrix =
    lines
        |> List.map ~f:parse_row
        |> List.to_array;;

let map_elements (f: int -> int) (matrix: matrix): matrix =
    Array.map ~f:(fun row -> Array.map ~f:f row) matrix;;

let map_elements_i (f: int -> int -> int -> int) (matrix: matrix): matrix =
    Array.mapi ~f:(fun y row -> Array.mapi ~f:(fun x el -> f x y el) row) matrix;;

let is_adjacent (x,y) (xx,yy): bool =
    let dx = Int.abs (xx - x) in
    let dy = Int.abs (yy - y) in
    (dx = 1 && dy <= 1) || (dy = 1 && dx <= 1);;

let step_matrix (matrix: matrix): matrix * int =
    let m1 = map_elements (fun el -> el + 1) matrix in
    let m2 = ref m1 in
    let flashed = ref [] in
    let continue = ref true in
    while !continue do
        continue := false;
        for x = 0 to 9 do
            for y = 0 to 9 do
                let energy: int = !m2.(y).(x) in
                let has_flashed = List.exists ~f:(fun (xx,yy) -> x = xx && y = yy) !flashed in
                if energy > 9 && not has_flashed then (
                    continue := true;
                    flashed := (x,y)::!flashed;
                    m2 := map_elements_i (fun xx yy e -> if is_adjacent (x,y) (xx,yy) then e + 1 else e) !m2;
                )
            done;
        done;
    done;
    let flashes = List.length !flashed in
    let m3 = map_elements (fun el -> if el > 9 then 0 else el) !m2 in
    (m3, flashes);;

let print_matrix (matrix: matrix) =
    for y = 0 to 9 do
        for x = 0 to 9 do
            let e = matrix.(y).(x) in
            printf "%d" e
        done;
        printf "\n"
    done;;

let m: matrix =
    "input.txt"
        |> In_channel.with_file ~f:In_channel.input_lines
        |> parse_matrix;;

let matrix = ref m in
let flashes = ref 0 in
for _ = 1 to 100 do
    let (m,f) = step_matrix !matrix in
    matrix := m;
    flashes := f + !flashes;
done;
print_matrix !matrix;
printf "Flashes: %d\n" !flashes;;
