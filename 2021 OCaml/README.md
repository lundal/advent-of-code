# OCaml

## Getting Started

1. `dnf install opam`
2. `opam init`
3. `opam install core dune`

## Running the Code

- For day 01 to 08 run `ocaml partX.ml`
- For day 09 to 25 run `dune exec ./partX.exe`

## References

### Core
- [Core](https://ocaml.janestreet.com/ocaml-core/latest/doc/core_kernel/Core_kernel/index.html)
- [Int](https://ocaml.janestreet.com/ocaml-core/latest/doc/core_kernel/Core_kernel/Int/index.html)
- [String](https://ocaml.janestreet.com/ocaml-core/latest/doc/core_kernel/Core_kernel/String/index.html)
- [Option](https://ocaml.janestreet.com/ocaml-core/latest/doc/core_kernel/Core_kernel/Option/index.html)
- [List](https://ocaml.janestreet.com/ocaml-core/latest/doc/core_kernel/Core_kernel/List/index.html)
- [Map](https://ocaml.janestreet.com/ocaml-core/latest/doc/core_kernel/Core_kernel/Map/index.html)
- [Printf](https://ocaml.janestreet.com/ocaml-core/latest/doc/core_kernel/Core_kernel/Printf/index.html)

### Stdio
- [In_channel](https://ocaml.janestreet.com/ocaml-core/latest/doc/stdio/Stdio/In_channel/index.html)
- [Out_channel](https://ocaml.janestreet.com/ocaml-core/latest/doc/stdio/Stdio/Out_channel/index.html)

