open Core;;
open Stdio;;

type position = int * int

module Position = struct
    module T = struct
        type t = position
        let compare = Tuple2.compare ~cmp1:Int.compare ~cmp2:Int.compare
        let sexp_of_t = Tuple2.sexp_of_t Int.sexp_of_t Int.sexp_of_t
        let t_of_sexp = Tuple2.t_of_sexp Int.t_of_sexp Int.t_of_sexp
    end
    include T
    include Comparable.Make(T)
end

type heightmap = (Position.t, int, Position.comparator_witness) Map.t

let to_heightmap (lines: string list): heightmap =
    let map = ref (Map.empty (module Position)) in
    for y = 0 to List.length lines - 1 do
        let line = List.nth_exn lines y in
        for x = 0 to String.length line - 1 do
            let char = String.get line x in
            let value = Int.of_string (Char.to_string char) in
            map := Map.set !map ~key:(x, y) ~data:value;
        done;
    done;
    !map;;

let adjacent_positions ((x,y): position): position list =
    [(x-1,y); (x+1,y); (x,y-1); (x,y+1)];;

let adjacent_values (map: heightmap) (pos: position): int list =
    adjacent_positions pos
        |> List.filter_map ~f:(Map.find map);;

let is_low_point (map: heightmap) (pos: position): bool =
    let value = Map.find_exn map pos in
    pos
        |> adjacent_values map
        |> List.for_all ~f:(fun v -> v > value);;

let rec find_basin (map: heightmap) (pos: position): position list =
    let value = Map.find_exn map pos in
    let is_in_basin (p: position): bool =
        match Map.find map p with
        | Some(v) -> v > value && v < 9
        | None -> false in
    let positions: position list =
        adjacent_positions pos
            |> List.filter ~f:is_in_basin
            |> List.concat_map ~f:(find_basin map) in
    pos::positions;;

let basin_size (map: heightmap) (pos: position): int =
    find_basin map pos
        |> List.dedup_and_sort ~compare:Position.compare
        |> List.length;;

let map: heightmap =
    "input.txt"
        |> In_channel.with_file ~f:In_channel.input_lines
        |> to_heightmap;;

let basin_sizes: int list =
    map
        |> Map.filter_keys ~f:(is_low_point map)
        |> Map.keys
        |> List.map ~f:(basin_size map)
        |> List.sort ~compare:Int.compare
        |> List.rev;;

printf "Basin sizes: %s\n" (List.to_string ~f:Int.to_string basin_sizes);;

match basin_sizes with
| b1::b2::b3::_ -> printf "Result: %d\n" (b1 * b2 * b3)
| _ -> printf "Not enough basins"
