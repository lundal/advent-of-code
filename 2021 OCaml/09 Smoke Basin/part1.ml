open Core;;
open Stdio;;

type position = int * int

module Position = struct
    module T = struct
        type t = position
        let compare = Tuple2.compare ~cmp1:Int.compare ~cmp2:Int.compare
        let sexp_of_t = Tuple2.sexp_of_t Int.sexp_of_t Int.sexp_of_t
        let t_of_sexp = Tuple2.t_of_sexp Int.t_of_sexp Int.t_of_sexp
    end
    include T
    include Comparable.Make(T)
end

type heightmap = (Position.t, int, Position.comparator_witness) Map.t

let to_heightmap (lines: string list): heightmap =
    let map = ref (Map.empty (module Position)) in
    for y = 0 to List.length lines - 1 do
        let line = List.nth_exn lines y in
        for x = 0 to String.length line - 1 do
            let char = String.get line x in
            let value = Int.of_string (Char.to_string char) in
            map := Map.set !map ~key:(x, y) ~data:value;
        done;
    done;
    !map;;

let adjacent_positions ((x,y): position): position list =
    [(x-1,y); (x+1,y); (x,y-1); (x,y+1)];;

let adjacent_values (map: heightmap) (pos: position): int list =
    adjacent_positions pos
        |> List.filter_map ~f:(Map.find map);;

let is_low_point (map: heightmap) (pos: position): bool =
    let value = Map.find_exn map pos in
    pos
        |> adjacent_values map
        |> List.for_all ~f:(fun v -> v > value);;

let to_risk_level (v: int): int =
    v + 1;;

let map: heightmap =
    "input.txt"
        |> In_channel.with_file ~f:In_channel.input_lines
        |> to_heightmap;;

let total_risk_level: int =
    map
        |> Map.filter_keys ~f:(is_low_point map)
        |> Map.map ~f:to_risk_level
        |> Map.data
        |> List.fold ~init:0 ~f:(+);;

printf "Risk: %d\n" total_risk_level;;
