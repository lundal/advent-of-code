let rec read_lines (file: in_channel): string list =
    try
        let line = Stdlib.input_line file in
        line :: read_lines file
    with Stdlib.End_of_file ->
        Stdlib.close_in file;
        [];;

let parse_state (line: string): int list =
    let days = line
        |> String.split_on_char ','
        |> List.map Stdlib.int_of_string in
    let state = ref [] in
    for day = 8 downto 0 do
        let day_count = days
            |> List.filter (fun d -> d = day)
            |> List.length in
        state := day_count :: !state;
    done;
    !state;;

let evolve_state (state: int list): int list =
    let d0::d1::d2::d3::d4::d5::d6::d7::d8::[] = state in
    d1::d2::d3::d4::d5::d6::(d7+d0)::d8::d0::[];;

let rec evolve_state_n (n: int) (state: int list): int list =
    match n with
    | 0 -> state
    | _ -> evolve_state_n (n-1) (evolve_state state);;

let sum_fish (state: int list): int =
    List.fold_left (+) 0 state;;

let state = read_lines (Stdlib.open_in "input.txt")
    |> List.hd
    |> parse_state
    |> evolve_state_n 256;;

Format.printf "Fish: %d\n" (sum_fish state);;
