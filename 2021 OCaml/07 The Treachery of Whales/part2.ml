let rec read_lines (file: in_channel): string list =
    try
        let line = Stdlib.input_line file in
        line :: read_lines file
    with Stdlib.End_of_file ->
        Stdlib.close_in file;
        [];;

let (--) (a: int) (b: int): int list =
    let rec inner i acc =
        if i < a then acc
        else inner (i - 1) (i :: acc)
    in inner b [];;

let min (list: int list): int =
    let head::tail = list in
    List.fold_left Stdlib.min head tail;;

let max (list: int list): int =
    let head::tail = list in
    List.fold_left Stdlib.max head tail;;

let parse_positions (line: string): int list =
    line
        |> String.split_on_char ','
        |> List.map Stdlib.int_of_string;;

let sum_fuel (positions: int list) (position: int): int =
    positions
        |> List.map (fun pos -> Stdlib.abs (pos - position))
        |> List.map (fun n -> (1 + n) * n / 2)
        |> List.fold_left (+) 0;;

let min_fuel (positions: int list): int =
    (min positions -- max positions)
        |> List.map (sum_fuel positions)
        |> min;;

let fuel = read_lines (Stdlib.open_in "input.txt")
    |> List.hd
    |> parse_positions
    |> min_fuel;;

Format.printf "Fuel: %d\n" fuel;;
