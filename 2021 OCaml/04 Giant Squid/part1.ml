#load "str.cma";;

let rec read_lines (file: in_channel): string list =
    try
        let line = Stdlib.input_line file in
        line :: read_lines file
    with Stdlib.End_of_file ->
        Stdlib.close_in file;
        [];;

let split (separator: 'a -> bool) (list: 'a list): 'a list list =
    let groups = ref [] in
    let group = ref [] in
    for i = List.length list - 1 downto 0 do
        let item = List.nth list i in
        if separator item then (
            groups := !group :: !groups;
            group := [];
        ) else (
            group := item :: !group;
        )
    done;
    !group :: !groups;;

let contains (item: 'a) (list: 'a list): bool =
    List.exists (fun i -> i = item) list;;

let rec transpose (list: 'a list list): 'a list list =
    match list with
    | [] -> []
    | []::tail -> transpose tail
    | _ -> (List.map List.hd list) :: transpose (List.map List.tl list)

let parse_numbers (line: string): int list =
    Str.split (Str.regexp "[ ,]+") line
    |> List.map Stdlib.int_of_string;;

type board = int list list;;

let parse_boards (lines: string list): board list =
    let groups = split (fun line -> line = "") lines in
    let map_group group = List.map parse_numbers group in
    List.map map_group groups;;

let is_bingo (draws: int list) (board: int list list): bool =
    let is_marked (square: int): bool = contains square draws in
    let is_complete_line (line: int list): bool = List.for_all is_marked line in
    List.exists is_complete_line board || List.exists is_complete_line (transpose board);;

let score_bingo (draws: int list) (board: int list list): int =
    let is_unmarked (square: int): bool = not (contains square draws) in
    let sum =
        List.flatten board
        |> List.filter is_unmarked
        |> List.fold_left (+) 0 in
    sum * (List.hd draws);;

let rec play_bingo (boards: board list) (draws: int list) (pending_draws: int list): int =
    match List.filter (is_bingo draws) boards with
    | board::[] -> score_bingo draws board
    | _ ->
        match pending_draws with
        | [] -> Stdlib.failwith "no more draws"
        | head::tail -> play_bingo boards (head::draws) tail;;

let lines = read_lines (Stdlib.open_in "input.txt");;
let draw_line::""::board_lines = lines;;
let draws = parse_numbers draw_line;;
let boards = parse_boards board_lines;;
Format.printf "Score: %d\n" (play_bingo boards [] draws);;
